package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.enums.UserSortOptions;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public User getByUsername(String username) {
        return getByFieldMatches("username", username)
                .orElseThrow(() -> new EntityNotFoundException("User", "username", username));
    }

    @Override
    public User getByEmail(String email) {
        return getByFieldMatches("email", email)
                .orElseThrow(() -> new EntityNotFoundException("User", "email", email));
    }

    @Override
    public User getByPhone(String phone){
        return getByFieldMatches("phoneNumber", phone)
                .orElseThrow(() -> new EntityNotFoundException("User", "phone number", phone));
    }

    @Override
    public User findByResetPasswordToken(String token) {
        return getByFieldMatches("resetPasswordToken", token)
                .orElseThrow(() -> new EntityNotFoundException("ResetPasswordToken", "resetPasswordToken", token));
    }

    @Override
    public List<User> filter(Optional<String> username,
                             Optional<String> email,
                             Optional<String> phoneNumber,
                             Optional<String> brand,
                             Optional<String> model,
                             Optional<String> licensePlate,
                             Optional<LocalDate> startDate,
                             Optional<LocalDate> endDate,
                             Optional<UserSortOptions> sort) {
        try (Session session = getSessionFactory().openSession()) {
            List<String> filter = new ArrayList<>();
            HashMap<String, Object> queryParams = new HashMap<>();
            StringBuilder queryBuilder = new StringBuilder("select distinct u from User u left join Vehicle v on v.owner = u.id left join Booking b on b.vehicle = v.id");

            username.ifPresent(value -> {
                filter.add(" username like :username ");
                queryParams.put("username", "%" + value + "%");
            });

            email.ifPresent(value -> {
                filter.add(" email like :email ");
                queryParams.put("email", "%" + value + "%");
            });

            phoneNumber.ifPresent(value -> {
                filter.add(" u.phoneNumber like :phoneNumber ");
                queryParams.put("phoneNumber", "%" + value + "%");
            });

            brand.ifPresent(value -> {
                filter.add(" v.model.brand.name like :brand ");
                queryParams.put("brand", "%" + value + "%");
            });

            model.ifPresent(value -> {
                filter.add(" v.model.name like :model ");
                queryParams.put("model", "%" + value + "%");
            });

            licensePlate.ifPresent(value -> {
                filter.add(" v.licensePlate = :lp ");
                queryParams.put("lp", value);
            });

            startDate.ifPresent(value -> {
                filter.add(" b.dropOffDate >= :startDate ");
                queryParams.put("startDate", value);
            });

            endDate.ifPresent(value -> {
                filter.add(" b.dropOffDate <= :endDate ");
                queryParams.put("endDate", value);
            });

            if (!filter.isEmpty()) {
                queryBuilder.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> queryBuilder.append(value.getQuery()));

            Query<User> queryList = session.createQuery(queryBuilder.toString(), User.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    @Override
    public List<User> search(Optional<String> search) {
        if (search.isEmpty()) return getAll();
        try (Session session = getSessionFactory().openSession()) {
            Query<User> query = session.createQuery("select u from User u left join Vehicle v on v.owner.id = u.id where u.username like :username or u.email like :email or u.phoneNumber like :phoneNumber or v.licensePlate like :licensePlate ", User.class);
            query.setParameter("username", "%" + search.get() + "%");
            query.setParameter("email", "%" + search.get() + "%");
            query.setParameter("phoneNumber", "%" + search.get() + "%");
            query.setParameter("licensePlate", "%" + search.get() + "%");
            return query.list();
        }
    }

}