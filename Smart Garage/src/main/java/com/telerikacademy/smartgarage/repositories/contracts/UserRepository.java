package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.enums.UserSortOptions;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.user.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseModifyRepository<User>{

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhone(String phone);

    User findByResetPasswordToken(String token);


    List<User> filter(Optional<String> username,
                      Optional<String> email,
                      Optional<String> phoneNumber,
                      Optional<String> brand,
                      Optional<String> model,
                      Optional<String> licensePlate,
                      Optional<LocalDate> startDate,
                      Optional<LocalDate> endDate,
                      Optional<UserSortOptions> sort);

    List<User> search(Optional<String> search);
}