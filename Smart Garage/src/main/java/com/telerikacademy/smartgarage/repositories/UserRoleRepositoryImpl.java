package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.enums.UserSortOptions;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.user.UserRole;
import com.telerikacademy.smartgarage.repositories.contracts.UserRoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRoleRepositoryImpl extends BaseGetRepositoryImpl<UserRole> implements UserRoleRepository {

    @Autowired
    public UserRoleRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRole.class, sessionFactory);
    }

}