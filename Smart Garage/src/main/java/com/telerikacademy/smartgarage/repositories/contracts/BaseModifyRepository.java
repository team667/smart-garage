package com.telerikacademy.smartgarage.repositories.contracts;

public interface BaseModifyRepository<T> extends BaseGetRepository<T> {

    void create(T entity);

    void update(T entity);

    void delete(T entity);

}