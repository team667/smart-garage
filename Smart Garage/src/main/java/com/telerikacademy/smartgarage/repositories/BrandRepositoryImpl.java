package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.repositories.contracts.BrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BrandRepositoryImpl extends BaseModifyRepositoryImpl<Brand> implements BrandRepository {

    @Autowired
    public BrandRepositoryImpl(SessionFactory sessionFactory) {
        super(Brand.class, sessionFactory);
    }

    @Override
    public Brand getByName(String name) {
        return getByFieldMatches("name", name)
                .orElseThrow(() -> new EntityNotFoundException("Brand", "name", name));
    }

    @Override
    public List<Brand> getAllPaginated(int pageSize, int currentPage) {
        try (Session session = getSessionFactory().openSession()) {
            Query<Brand> selectQuery = session.createQuery("select b from Brand b ", Brand.class);
            selectQuery.setFirstResult((currentPage - 1) * pageSize);
            selectQuery.setMaxResults(pageSize);
            return selectQuery.list();
        }
    }

    @Override
    public int lastPageNumber(int pageSize) {
        try (Session session = getSessionFactory().openSession()) {
            Query queryCount = session.createQuery("select count(b.id) from Brand b ");
            Long countResults = (Long) queryCount.uniqueResult();
            return (int) (Math.ceil((double) countResults / (double) pageSize));
        }
    }

}