package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.brand.Brand;

import java.util.List;

public interface BrandRepository extends BaseModifyRepository<Brand> {

    Brand getByName(String username);

    List<Brand> getAllPaginated(int pageSize, int currentPage);

    int lastPageNumber(int pageSize);
}