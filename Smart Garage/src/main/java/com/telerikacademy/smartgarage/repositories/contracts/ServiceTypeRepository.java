package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.enums.ServiceTypeSortOptions;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;

import java.util.List;
import java.util.Optional;

public interface ServiceTypeRepository extends BaseModifyRepository<ServiceType> {

    List<ServiceType> filter(Optional<String> name, Optional<ServiceTypeSortOptions> sort);

    List<ServiceType> getAllPaginated(int pageSize, int currentPage);

    int lastPageNumber(int pageSize);

}