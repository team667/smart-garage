package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.enums.ServiceTypeSortOptions;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceTypeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ServiceTypeRepositoryImpl extends BaseModifyRepositoryImpl<ServiceType> implements ServiceTypeRepository {

    @Autowired
    public ServiceTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(ServiceType.class, sessionFactory);
    }

    @Override
    public List<ServiceType> getAllPaginated(int pageSize, int currentPage) {
        try (Session session = getSessionFactory().openSession()) {
            Query<ServiceType> selectQuery = session.createQuery("select st from ServiceType st ", ServiceType.class);
            selectQuery.setFirstResult((currentPage - 1) * pageSize);
            selectQuery.setMaxResults(pageSize);
            return selectQuery.list();
        }
    }

    @Override
    public int lastPageNumber(int pageSize) {
        try (Session session = getSessionFactory().openSession()) {
            Query queryCount = session.createQuery("select count(st.id) from ServiceType st ");
            Long countResults = (Long) queryCount.uniqueResult();
            return (int) (Math.ceil( (double) countResults / (double) pageSize));
        }
    }

    @Override
    public List<ServiceType> filter(Optional<String> name, Optional<ServiceTypeSortOptions> sort) {
        try (Session session = getSessionFactory().openSession()) {
            List<String> filter = new ArrayList<>();
            HashMap<String, Object> queryParams = new HashMap<>();
            StringBuilder queryBuilder = new StringBuilder("select st from ServiceType st ");

            name.ifPresent(value -> {
                filter.add(" st.name like :name ");
                queryParams.put("name", "%" + value + "%");
            });

            if (!filter.isEmpty()) {
                queryBuilder.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> queryBuilder.append(value.getQuery()));

            Query<ServiceType> query = session.createQuery(queryBuilder.toString(), ServiceType.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

}