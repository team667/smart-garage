package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceCategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceCategoryRepositoryImpl extends BaseModifyRepositoryImpl<ServiceCategory> implements ServiceCategoryRepository {

    @Autowired
    public ServiceCategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(ServiceCategory.class, sessionFactory);
    }

}