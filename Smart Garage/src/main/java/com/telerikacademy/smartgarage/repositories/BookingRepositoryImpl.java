package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.enums.BookingSortOptions;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.repositories.contracts.BookingRepository;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
public class BookingRepositoryImpl extends BaseModifyRepositoryImpl<Booking> implements BookingRepository {

    @Autowired
    public BookingRepositoryImpl(SessionFactory sessionFactory) {
        super(Booking.class, sessionFactory);
    }

    @Override
    public List<Booking> filter(Optional<Integer> userId,
                                Optional<Integer> modelId,
                                Optional<LocalDate> timestamp,
                                Optional<BookingSortOptions> sort,
                                User customer) {
        try (Session session = getSessionFactory().openSession()) {
            StringBuilder query = new StringBuilder("select b from Booking b ");
            List<String> filter = new ArrayList<>();
            Map<String, Object> queryParams = new HashMap<>();

            userId.ifPresent(value ->{
                filter.add(" vehicle.owner.id = :ownerId ");
                queryParams.put("ownerId", value);
            });

            modelId.ifPresent(value -> {
                filter.add(" vehicle.model.id like :modelId ");
                queryParams.put("modelId", value);
            });

            timestamp.ifPresent(value -> {
                filter.add(" dropOffDate >= :date ");
                queryParams.put("date", value);
            });

            if (!customer.isEmployee()){
                filter.add(" vehicle.owner.id = :customerId ");
                queryParams.put("customerId", customer.getId());
            }

            if (!filter.isEmpty()) {
            query.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> query.append(value.getQuery()));

            Query<Booking> queryList = session.createQuery(query.toString(), Booking.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

}