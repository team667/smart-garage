package com.telerikacademy.smartgarage.repositories.contracts;

import java.util.List;
import java.util.Optional;

public interface BaseGetRepository<T> {

    List<T> getAll();

    T getById(int id);

    <V> Optional<T> getByFieldMatches(String fieldName, V value);

    <V> List<T> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<T> getByFieldContains(String fieldName, V value);

    <V> List<T> getAllByFieldContains(String fieldName, V value);

}