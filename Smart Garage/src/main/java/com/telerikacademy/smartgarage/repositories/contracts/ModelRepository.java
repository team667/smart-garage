package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.user.User;

import java.util.List;

public interface ModelRepository extends BaseModifyRepository<Model> {

    List<Model> getAllPaginated(int pageSize, int currentPage);

    int lastPageNumber(int pageSize);

    Model getByName(String name);

    List<Model> getByBrandId(int id);
}