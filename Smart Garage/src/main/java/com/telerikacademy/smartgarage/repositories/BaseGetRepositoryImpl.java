package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.repositories.contracts.BaseGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.*;

public abstract class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    private final Class<T> entityClass;
    private final SessionFactory sessionFactory;

    public BaseGetRepositoryImpl(Class<T> entityClass, SessionFactory sessionFactory) {
        this.entityClass = entityClass;
        this.sessionFactory = sessionFactory;
    }

    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from " + entityClass.getSimpleName(), entityClass).list();
        }
    }

    @Override
    public T getById(int id) {
        return getByFieldMatches("id", id)
                .orElseThrow(() -> new EntityNotFoundException(entityClass.getSimpleName(), id));
    }

    @Override
    public <V> Optional<T> getByFieldMatches(String fieldName, V value) {
        try (Session session = sessionFactory.openSession()) {
            return getQueryByFieldNameMatches(fieldName, value, session).uniqueResultOptional();
        }
    }

    @Override
    public <V> List<T> getAllByFieldMatches(String fieldName, V value) {
        try (Session session = sessionFactory.openSession()) {
            return getQueryByFieldNameMatches(fieldName, value, session).list();
        }
    }

    @Override
    public <V> Optional<T> getByFieldContains(String fieldName, V value) {
        try (Session session = sessionFactory.openSession()) {
            return getQueryByFieldNameContains(fieldName, value, session).uniqueResultOptional();
        }
    }

    @Override
    public <V> List<T> getAllByFieldContains(String fieldName, V value) {
        try (Session session = sessionFactory.openSession()) {
            return getQueryByFieldNameContains(fieldName, value, session).list();
        }
    }

    private <V> Query<T> getQueryByFieldNameMatches(String fieldName, V value, Session session) {
        return session.createQuery(String.format("from %s where %s = :value", entityClass.getSimpleName(), fieldName), entityClass)
                .setParameter("value", value);
    }

    private <V> Query<T> getQueryByFieldNameContains(String fieldName, V value, Session session) {
        return session.createQuery(String.format("from %s where %s like :value", entityClass.getSimpleName(), fieldName), entityClass)
                .setParameter("value", value);
    }

}