package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.enums.VehicleSortOptions;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends BaseModifyRepository<Vehicle> {

    Vehicle getByVin(String vin);

    List<Vehicle> getByUserId(int userId);

    List<Vehicle> filter(Optional<Integer> userId, Optional<VehicleSortOptions> sort);

    List<Vehicle> customerSearch(Optional<String> search, int userId);

    List<Vehicle> adminSearch(Optional<String> search);
}