package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.enums.BookingSortOptions;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.user.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BookingRepository extends BaseModifyRepository<Booking> {

    List<Booking> filter(Optional<Integer> userId,
                         Optional<Integer> modelId,
                         Optional<LocalDate> timestamp,
                         Optional<BookingSortOptions> sort,
                         User customer);

}