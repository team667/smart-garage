package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.enums.VehicleSortOptions;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class VehicleRepositoryImpl extends BaseModifyRepositoryImpl<Vehicle> implements VehicleRepository {

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        super(Vehicle.class, sessionFactory);
    }

    @Override
    public Vehicle getByVin(String vin) {
        return getByFieldContains("vin", vin)
                .orElseThrow(() -> new EntityNotFoundException("Vehicle", "vin", vin));
    }

    @Override
    public List<Vehicle> getByUserId(int userId) {
        return getAllByFieldMatches("owner.id", userId);
    }

    @Override
    public List<Vehicle> filter(Optional<Integer> userId, Optional<VehicleSortOptions> sort) {
        try (Session session = getSessionFactory().openSession()) {
            StringBuilder queryString = new StringBuilder("select v from Vehicle v ");
            HashMap<String, Object> queryParams = new HashMap<>();
            List<String> filter = new ArrayList<>();

            userId.ifPresent(value -> {
                filter.add(" owner.id = :userId ");
                queryParams.put("userId", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(value.getQuery());
            });

            Query<Vehicle> query = session.createQuery(queryString.toString(),Vehicle.class)
                    .setProperties(queryParams);
            return query.list();
        }
    }

    @Override
    public List<Vehicle> customerSearch(Optional<String> search, int userId) {
        if (search.isEmpty()) return getAll();
        try (Session session = getSessionFactory().openSession()) {
            Query<Vehicle> query = session.createQuery("select v from Vehicle v where v.owner.id = :userId and (v.licensePlate like :licensePlate or v.vin like :vin)", Vehicle.class);
            query.setParameter("userId", userId);
            query.setParameter("licensePlate", "%" + search.get() + "%");
            query.setParameter("vin", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Vehicle> adminSearch(Optional<String> search) {
        if (search.isEmpty()) return getAll();
        try (Session session = getSessionFactory().openSession()) {
            Query<Vehicle> query = session.createQuery("select v from Vehicle v join User u on u.id = v.owner.id where v.licensePlate like :licensePlate or v.vin like :vin or u.phoneNumber like :phoneNumber", Vehicle.class);
            query.setParameter("phoneNumber", "%" + search.get() + "%");
            query.setParameter("licensePlate", "%" + search.get() + "%");
            query.setParameter("vin", "%" + search.get() + "%");
            return query.list();
        }
    }

}