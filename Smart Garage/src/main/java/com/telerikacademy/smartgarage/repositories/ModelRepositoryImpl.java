package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl extends BaseModifyRepositoryImpl<Model> implements ModelRepository {

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        super(Model.class, sessionFactory);
    }

    @Override
    public List<Model> getAllPaginated(int pageSize, int currentPage) {
        try (Session session = getSessionFactory().openSession()) {
            Query<Model> selectQuery = session.createQuery("select m from Model m ", Model.class);
            selectQuery.setFirstResult((currentPage - 1) * pageSize);
            selectQuery.setMaxResults(pageSize);
            return selectQuery.list();
        }
    }

    @Override
    public int lastPageNumber(int pageSize) {
        try (Session session = getSessionFactory().openSession()) {
            Query queryCount = session.createQuery("select count(m.id) from Model m ");
            Long countResults = (Long) queryCount.uniqueResult();
            return (int) (Math.ceil( (double) countResults / (double) pageSize));
        }
    }

    @Override
    public Model getByName(String name) {
        return getByFieldMatches("name", name)
                .orElseThrow(() -> new EntityNotFoundException("Model", "name", name));
    }

    @Override
    public List<Model> getByBrandId(int id) {
        return getAllByFieldMatches("brand.id", id);
    }

}