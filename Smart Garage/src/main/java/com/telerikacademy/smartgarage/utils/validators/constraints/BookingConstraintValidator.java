package com.telerikacademy.smartgarage.utils.validators.constraints;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.hibernate.validator.messageinterpolation.ExpressionLanguageFeatureLevel;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

//public class BookingConstraintValidator implements ConstraintValidator<Booking, LocalDateTime> {
public class BookingConstraintValidator implements ConstraintValidator<Booking, LocalDate> {
    @Override
    public void initialize(Booking constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        LocalDate minDate = LocalDate.now();
        LocalDate maxDate = LocalDate.now().plusMonths(1);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("d MMMM yyyy");
        if (value.isBefore(minDate) || value.isAfter(maxDate)){
            HibernateConstraintValidatorContext hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);

            hibernateContext.disableDefaultConstraintViolation();
            hibernateContext.addExpressionVariable("minDate", df.format(minDate))
                    .addExpressionVariable("maxDate", df.format(maxDate))
                    .buildConstraintViolationWithTemplate("Drop off date must be between today and ${maxDate}")
                    .enableExpressionLanguage(ExpressionLanguageFeatureLevel.BEAN_PROPERTIES)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

}