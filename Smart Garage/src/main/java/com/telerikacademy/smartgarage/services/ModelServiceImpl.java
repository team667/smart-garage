package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll() {
        return modelRepository.getAll();
    }

    @Override
    public List<Model> getAllPaginated(int pageSize, int currentPage) {
        return modelRepository.getAllPaginated(pageSize, currentPage);
    }

    @Override
    public int lastPageNumber(int pageSize) {
        return modelRepository.lastPageNumber(pageSize);
    }

    @Override
    public List<Model> getByBrandId(int brandId) {
        return modelRepository.getByBrandId(brandId);
    }

    @Override
    public Model getById(int id) {
        return modelRepository.getById(id);
    }

    @Override
    public <V> Optional<Model> getByFieldMatches(String fieldName, V value) {
        return modelRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<Model> getAllByFieldMatches(String fieldName, V value) {
        return modelRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<Model> getByFieldContains(String fieldName, V value) {
        return modelRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<Model> getAllByFieldContains(String fieldName, V value) {
        return modelRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public void create(Model entity) {
        boolean vehicleExist = true;
        try {
            modelRepository.getByName(entity.getName());
        } catch (EntityNotFoundException e) {
            vehicleExist = false;
        }
        if (vehicleExist) {
            throw new DuplicateEntityException("Model", "name", entity.getName());
        }
        modelRepository.create(entity);

    }

    @Override
    public void update(Model entity) {
        boolean duplicateExists = true;
        try {
            Model existingModel = modelRepository.getByName(entity.getName());
            if (existingModel.getId() == entity.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) throw new DuplicateEntityException("Brand", "name", entity.getName());

        modelRepository.update(entity);
    }

    @Override
    public void delete(Model model) {
        modelRepository.delete(model);
    }

}