package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository serviceRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @Override
    public List<com.telerikacademy.smartgarage.models.service.Service> getAll() {
        return serviceRepository.getAll();
    }

    @Override
    public com.telerikacademy.smartgarage.models.service.Service getById(int id) {
        return serviceRepository.getById(id);
    }

    @Override
    public <V> Optional<com.telerikacademy.smartgarage.models.service.Service> getByFieldMatches(String fieldName, V value) {
        return serviceRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<com.telerikacademy.smartgarage.models.service.Service> getAllByFieldMatches(String fieldName, V value) {
        return serviceRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<com.telerikacademy.smartgarage.models.service.Service> getByFieldContains(String fieldName, V value) {
        return serviceRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<com.telerikacademy.smartgarage.models.service.Service> getAllByFieldContains(String fieldName, V value) {
        return serviceRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public void create(com.telerikacademy.smartgarage.models.service.Service service, User requester) {
        if (!requester.isEmployee() && requester.getId() != service.getBooking().getVehicle().getOwner().getId()) {
            throw new UnauthorisedOperationException("You're not authorised to add services to this booking");
        }
        boolean serviceExists = true;
        try {
            //TODO duplicate logic
        } catch (EntityNotFoundException e) {
            serviceExists = false;
        }
        serviceRepository.create(service);
    }

    @Override
    public void update(com.telerikacademy.smartgarage.models.service.Service service, User requester) {
        if (!requester.isEmployee() && requester.getId() != service.getBooking().getVehicle().getOwner().getId()) {
            throw new UnauthorisedOperationException("You're not authorised to add services to this booking");
        }
        serviceRepository.update(service);
    }

    @Override
    public void delete(com.telerikacademy.smartgarage.models.service.Service service, User requester) {
        if (!requester.isEmployee() && requester.getId() != service.getBooking().getVehicle().getOwner().getId()) {
            throw new UnauthorisedOperationException("You're not authorised to add services to this booking");
        }
        serviceRepository.delete(service);
    }

}