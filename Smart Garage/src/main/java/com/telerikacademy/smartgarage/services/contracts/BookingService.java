package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.enums.BookingSortOptions;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.booking.Booking;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BookingService {

    List<Booking> getAll();

    Booking getById(int id);

    <V> Optional<Booking> getByFieldMatches(String fieldName, V value);

    <V> List<Booking> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Booking> getByFieldContains(String fieldName, V value);

    <V> List<Booking> getAllByFieldContains(String fieldName, V value);

    void create(Booking entity);

    void update(Booking entity, User requester);

    void delete(Booking entity, User requester);

    List<Booking> filter(Optional<Integer> userId,
                         Optional<Integer> modelId,
                         Optional<LocalDate> timestamp,
                         Optional<BookingSortOptions> sort,
                         User customer);
}