package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.enums.VehicleSortOptions;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Vehicle> getAll() {
        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getById(int id) {
        return vehicleRepository.getById(id);
    }

    @Override
    public List<Vehicle> getByUserId(int userId) {
        return vehicleRepository.getByUserId(userId);
    }

    @Override
    public <V> Optional<Vehicle> getByFieldMatches(String fieldName, V value) {
        return vehicleRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<Vehicle> getAllByFieldMatches(String fieldName, V value) {
        return vehicleRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<Vehicle> getByFieldContains(String fieldName, V value) {
        return vehicleRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<Vehicle> getAllByFieldContains(String fieldName, V value) {
        return vehicleRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public List<Vehicle> filter(Optional<Integer> userId, Optional<VehicleSortOptions> sort, User requester) {
        if (!requester.isEmployee()) {
            throw new UnauthorisedOperationException("You're not authorised to access the requested resource");
        }
        return vehicleRepository.filter(userId, sort);
    }

    @Override
    public List<Vehicle> customerSearch(Optional<String> search, int userId) {
        return vehicleRepository.customerSearch(search, userId);
    }

    @Override
    public List<Vehicle> adminSearch(Optional<String> search) {
        return vehicleRepository.adminSearch(search);
    }

    @Override
    public void create(Vehicle entity) {
        boolean vehicleExist = true;
        try {
            vehicleRepository.getByVin(entity.getVin());
        } catch (EntityNotFoundException e) {
            vehicleExist = false;
        }
        if (vehicleExist) {
            throw new DuplicateEntityException("Vehicle with this vin already exists");
        }
        vehicleRepository.create(entity);
    }

    @Override
    public void update(Vehicle entity) {
        boolean duplicateExists = true;
        try {
            Vehicle existingVehicle = vehicleRepository.getByVin(entity.getVin());
            if (existingVehicle.getId() == entity.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) throw new DuplicateEntityException("Vehicle", "vin", entity.getVin());

        vehicleRepository.update(entity);
    }

    @Override
    public void delete(Vehicle entity) {
        vehicleRepository.delete(entity);
    }

}