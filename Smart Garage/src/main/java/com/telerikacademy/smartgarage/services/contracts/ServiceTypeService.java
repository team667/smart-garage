package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.enums.ServiceTypeSortOptions;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.user.User;

import java.util.List;
import java.util.Optional;

public interface ServiceTypeService {

    List<ServiceType> getAll();

    List<ServiceType> getAllPaginated(int pageSize, int currentPage);

    List<ServiceType> filter(Optional<String> name, Optional<ServiceTypeSortOptions> sort);

    ServiceType getById(int id);

    <V> Optional<ServiceType> getByFieldMatches(String fieldName, V value);

    <V> List<ServiceType> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<ServiceType> getByFieldContains(String fieldName, V value);

    <V> List<ServiceType> getAllByFieldContains(String fieldName, V value);

    void create(ServiceType entity, User requester);

    void update(ServiceType entity, User requester);

    void delete(ServiceType entity, User requester);

    int lastPageNumber(int pageSize);

}