package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.enums.VehicleSortOptions;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleService {

    List<Vehicle> getAll();

    Vehicle getById(int id);

    List<Vehicle> getByUserId(int userId);

    <V> Optional<Vehicle> getByFieldMatches(String fieldName, V value);

    <V> List<Vehicle> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Vehicle> getByFieldContains(String fieldName, V value);

    <V> List<Vehicle> getAllByFieldContains(String fieldName, V value);

    List<Vehicle> filter(Optional<Integer> userId, Optional<VehicleSortOptions> sort, User requester);

    List<Vehicle> customerSearch(Optional<String> search, int userId);

    List<Vehicle> adminSearch(Optional<String> search);

    void create(Vehicle entity);

    void update(Vehicle entity);

    void delete(Vehicle entity);

}