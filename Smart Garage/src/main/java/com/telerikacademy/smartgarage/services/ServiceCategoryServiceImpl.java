package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceCategoryRepository;
import com.telerikacademy.smartgarage.services.contracts.ServiceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceCategoryServiceImpl implements ServiceCategoryService {

    private final ServiceCategoryRepository serviceCategoryRepository;

    @Autowired
    public ServiceCategoryServiceImpl(ServiceCategoryRepository serviceCategoryRepository) {
        this.serviceCategoryRepository = serviceCategoryRepository;
    }


    @Override
    public List<ServiceCategory> getAll() {
        return serviceCategoryRepository.getAll();
    }

    @Override
    public ServiceCategory getById(int id) {
        return serviceCategoryRepository.getById(id);
    }

    @Override
    public <V> Optional<ServiceCategory> getByFieldMatches(String fieldName, V value) {
        return serviceCategoryRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<ServiceCategory> getAllByFieldMatches(String fieldName, V value) {
        return serviceCategoryRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<ServiceCategory> getByFieldContains(String fieldName, V value) {
        return serviceCategoryRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<ServiceCategory> getAllByFieldContains(String fieldName, V value) {
        return serviceCategoryRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public void create(ServiceCategory entity, User requester) {
        if (!requester.isEmployee()) throw new UnauthorisedOperationException("You're not authorised to create categories");
        serviceCategoryRepository.create(entity);
    }

    @Override
    public void update(ServiceCategory entity, User requester) {
        if (!requester.isEmployee()) throw new UnauthorisedOperationException("You're not authorised to update categories");
        serviceCategoryRepository.update(entity);
    }

    @Override
    public void delete(ServiceCategory serviceCategory, User requester) {
        if (!requester.isEmployee()) throw new UnauthorisedOperationException("You're not authorised to delete categories");
        serviceCategoryRepository.delete(serviceCategory);
    }

}