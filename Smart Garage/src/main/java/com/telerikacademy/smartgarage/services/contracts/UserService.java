package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.enums.UserSortOptions;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.user.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByResetPasswordToken(String token);

    void updateResetPasswordToken(String token, String email);

    void updatePassword(User user, String newPassword);

    User getByEmail(String email);

    User getByPhone(String phoneNumber);

    <V> Optional<User> getByFieldMatches(String fieldName, V value);

    <V> List<User> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<User> getByFieldContains(String fieldName, V value);

    <V> List<User> getAllByFieldContains(String fieldName, V value);

    void create(User entity);

    void update(User requester, User entity);

    void delete(User entity, User requester);

    List<User> filter(Optional<String> username,
                      Optional<String> email,
                      Optional<String> phoneNumber,
                      Optional<String> brand,
                      Optional<String> model,
                      Optional<String> licensePlate,
                      Optional<LocalDate> startDate,
                      Optional<LocalDate> endDate,
                      Optional<UserSortOptions> sort,
                      User requester);

    List<User> search(Optional<String> search, User requester);
}