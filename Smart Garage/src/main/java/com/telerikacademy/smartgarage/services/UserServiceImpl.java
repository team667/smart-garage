package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.enums.UserSortOptions;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
        User user = userRepository.getByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.update(user);
        } else {
            throw new EntityNotFoundException("User", "email", email);
        }
    }

    @Override
    public void updatePassword(User user, String newPassword) {
        user.setPassword(newPassword);
        user.setResetPasswordToken(null);
        userRepository.update(user);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByPhone(String phoneNumber) {
        return userRepository.getByPhone(phoneNumber);
    }

    @Override
    public <V> Optional<User> getByFieldMatches(String fieldName, V value) {
        return userRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<User> getAllByFieldMatches(String fieldName, V value) {
        return userRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<User> getByFieldContains(String fieldName, V value) {
        return userRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<User> getAllByFieldContains(String fieldName, V value) {
        return userRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public void create(User entity) {
        boolean usernameDuplicate = true;
        boolean emailDuplicate = true;
        boolean phoneDuplicate = true;
        try {
            userRepository.getByUsername(entity.getUsername());
        } catch (EntityNotFoundException e) {
            usernameDuplicate = false;
        }
        try {
            userRepository.getByEmail(entity.getEmail());
        } catch (EntityNotFoundException e) {
            emailDuplicate = false;
        }
        try {
            userRepository.getByPhone(entity.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            phoneDuplicate = false;
        }
        if (emailDuplicate && usernameDuplicate) {
            throw new DuplicateEntityException("User", "username and email", entity.getUsername(), entity.getEmail());
        }
        if (usernameDuplicate) {
            throw new DuplicateEntityException("User", "username", entity.getUsername());
        }
        if (emailDuplicate) {
            throw new DuplicateEntityException("User", "email", entity.getEmail());
        }
        if (phoneDuplicate){
            throw new DuplicateEntityException("User", "phone number", entity.getPhoneNumber());
        }
        userRepository.create(entity);
    }

    @Override
    public void update(User entity, User requester) {
        if (requester.isEmployee() || requester.getId() == entity.getId()) {
            boolean duplicateExists = true;
            try {
                User existingUser = userRepository.getByEmail(entity.getEmail());
                if (existingUser.getId() == entity.getId()) {
                    duplicateExists = false;
                }
            } catch (EntityNotFoundException e) {
                duplicateExists = false;
            }
            if (duplicateExists) throw new DuplicateEntityException("User", "username", entity.getUsername());
            userRepository.update(entity);
            return;
        }
        throw new UnauthorisedOperationException("You must log in with the user that you're trying to update.");
    }


    @Override
    public void delete(User entity, User requester) {
        if (!requester.isEmployee() && requester.getId() != entity.getId()) {
            throw new UnauthorisedOperationException("You must log in with the user that you're trying to delete.");
        }
        userRepository.delete(entity);
    }

    @Override
    public List<User> filter(Optional<String> username,
                             Optional<String> email,
                             Optional<String> phoneNumber,
                             Optional<String> brand,
                             Optional<String> model,
                             Optional<String> licensePlate,
                             Optional<LocalDate> startDate,
                             Optional<LocalDate> endDate,
                             Optional<UserSortOptions> sort,
                             User requester) {
        if (!requester.isEmployee()) {
            throw new UnauthorisedOperationException("You're not authorised to access the requested resource");
        }
        return userRepository.filter(username, email, phoneNumber, brand, model, licensePlate, startDate, endDate, sort);
    }

    @Override
    public List<User> search(Optional<String> search, User requester) {
        if (!requester.isEmployee()) {
            throw new UnauthorisedOperationException("You're not authorised to access the requested resource.");
        }
        return userRepository.search(search);
    }

}