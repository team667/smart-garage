package com.telerikacademy.smartgarage.config;

import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.config.Config;
import com.posadskiy.currencyconverter.config.ConfigBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CurrencyApiConfig {

    public static final String CURRENCY_CONVERTER_API_API_KEY = "d5f23a26c271f313c757";
    public static final String CURRENCY_LAYER = "e9326fb8016803d8d2b82e3ea78c8e3d";
    public static final String OPEN_EXCHANGE_RATES = "0c4dcf9e31094b4592457e7f36760c7d";


    @Bean
    public CurrencyConverter converter(){
        return new CurrencyConverter(new ConfigBuilder()
                        .currencyConverterApiApiKey(CURRENCY_CONVERTER_API_API_KEY)
                        .currencyLayerApiKey(CURRENCY_LAYER)
                        .openExchangeRatesApiKey(OPEN_EXCHANGE_RATES)
                        .build());

    }


}