package com.telerikacademy.smartgarage.enums;

import java.util.HashMap;
import java.util.Map;

public enum BookingSortOptions {

    VEHICLE_ASC("Vehicle, ascending", " order by b.vehicle.id "),
    VEHICLE_DESC("Vehicle, descending", " order by b.vehicle.id desc "),
    DATE_ASC("Date, ascending", " order by b.dropOffDate "),
    DATE_DESC("Date, descending", " order by b.dropOffDate desc ");

    private final String preview;
    private final String query;

    private static final Map<String, BookingSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (BookingSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    BookingSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static BookingSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }

}