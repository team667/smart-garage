package com.telerikacademy.smartgarage.enums;

import java.util.HashMap;
import java.util.Map;

public enum VehicleSortOptions {

    BRAND_ASC("Brand, ascending", " order by v.model.brand.name "),
    BRAND_DESC("Brand, descending", " order by v.model.brand.name desc ");

    private final String preview;
    private final String query;

    private static final Map<String, VehicleSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (VehicleSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    VehicleSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static VehicleSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }

}