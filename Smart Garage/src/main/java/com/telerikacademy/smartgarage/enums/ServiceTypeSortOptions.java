package com.telerikacademy.smartgarage.enums;

import java.util.HashMap;
import java.util.Map;

public enum ServiceTypeSortOptions {

    NAME_ASC("Name, ascending", " order by st.name "),
    NAME_DESC("Name, descending", " order by st.name desc ");

    private final String preview;
    private final String query;

    private static final Map<String, ServiceTypeSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (ServiceTypeSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    ServiceTypeSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static ServiceTypeSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }

}