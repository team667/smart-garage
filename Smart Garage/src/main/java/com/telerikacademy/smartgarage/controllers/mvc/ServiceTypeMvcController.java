package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.servicetype.ServiceTypeDTO;
import com.telerikacademy.smartgarage.models.servicetype.ServiceTypeMapper;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.ServiceCategoryService;
import com.telerikacademy.smartgarage.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/services")
@ApiIgnore
public class ServiceTypeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceTypeService serviceTypeService;
    private final ServiceCategoryService serviceCategoryService;
    private final ServiceTypeMapper serviceTypeMapper;

    @Autowired
    public ServiceTypeMvcController(AuthenticationHelper authenticationHelper,
                                    ServiceTypeService serviceTypeService,
                                    ServiceCategoryService serviceCategoryService,
                                    ServiceTypeMapper serviceTypeMapper) {
        this.authenticationHelper = authenticationHelper;
        this.serviceTypeService = serviceTypeService;
        this.serviceCategoryService = serviceCategoryService;
        this.serviceTypeMapper = serviceTypeMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isModifyingBooking")
    public boolean populateIsModifyingBooking(HttpSession httpSession) {
        return httpSession.getAttribute("bookingId") != null;
    }

    @GetMapping
    public String showAll(@RequestParam(required = false) Optional<Integer> p,
                          @RequestParam(required = false) Optional<Integer> l,
                          Model model,
                          HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("serviceTypeDTO", new ServiceTypeDTO());
            populateModelAttributes(model, l, p);
            return "services";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id,
                          Model model,
                          HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access the requested resource");
            }
            List<ServiceCategory> categories = serviceCategoryService.getAll();
            model.addAttribute("categories", categories);
            ServiceType serviceType = serviceTypeService.getById(id);
            model.addAttribute("serviceType", serviceType);
            ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.dtoFromObject(serviceType);
            model.addAttribute("serviceTypeDTO", serviceTypeDTO);
            return "service";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("serviceTypeDTO") ServiceTypeDTO serviceTypeDTO,
                         BindingResult bindingResult,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                populateModelAttributes(model, Optional.empty(), Optional.empty());
                return "services";
            }
            ServiceType serviceType = serviceTypeMapper.createFromDTO(serviceTypeDTO);
            serviceTypeService.create(serviceType, requester);
            return "redirect:/services?p=1&l=10";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    private void populateModelAttributes(Model model, Optional<Integer> l, Optional<Integer> p) {
        int pageSize = 0;
        int lastPage = 0;
        int currentPage = 1;
        List<ServiceType> serviceTypes;
        if (l.isEmpty()) {
            serviceTypes = serviceTypeService.getAll();
        } else {
            pageSize = l.get();
            lastPage = serviceTypeService.lastPageNumber(pageSize);
            if (p.isPresent()) {
                currentPage = p.get();
            }
            serviceTypes = serviceTypeService.getAllPaginated(pageSize, currentPage);
        }
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("lastPage", lastPage);
        model.addAttribute("servicesTypes", serviceTypes);
        List<ServiceCategory> categories = serviceCategoryService.getAll();
        model.addAttribute("categories", categories);
    }

//    @GetMapping("/{id}/edit")
//    public String showEditPage(@PathVariable int id,
//                               Model model,
//                               HttpSession httpSession) {
//        try {
//            User requester = authenticationHelper.tryGetUser(httpSession);
//            model.addAttribute("requester", requester);
//            ServiceType serviceTypeToUpdate = serviceTypeService.getById(id);
//            ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.dtoFromObject(serviceTypeToUpdate);
//            model.addAttribute("serviceType", serviceTypeDTO);
//            return "service-manager";
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
//            model.addAttribute("error", e.getMessage());
//            return "errors";
//        }
//    }

    @PostMapping("/{id}/edit")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("serviceTypeDTO") ServiceTypeDTO serviceTypeDTO,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            ServiceType serviceTypeToUpdate = serviceTypeService.getById(id);
            model.addAttribute("serviceType", serviceTypeToUpdate);
            if (bindingResult.hasErrors()) {
                List<ServiceCategory> categories = serviceCategoryService.getAll();
                model.addAttribute("categories", categories);
                return "service";
            }
            ServiceType updatedServiceType = serviceTypeMapper.updateFromDTO(serviceTypeDTO, serviceTypeToUpdate);
            serviceTypeService.update(updatedServiceType, requester);
            return "redirect:/services/" + id;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            ServiceType serviceTypeToDelete = serviceTypeService.getById(id);
            serviceTypeService.delete(serviceTypeToDelete, requester);
            return "redirect:/services?p=1&l=10";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

}