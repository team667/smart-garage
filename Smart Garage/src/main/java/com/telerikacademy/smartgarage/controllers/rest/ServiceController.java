package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.service.ServiceMapper;
import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.models.service.ServiceDTO;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.BookingService;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.smartgarage.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/booking-services")
public class ServiceController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceService serviceService;
    private final ServiceMapper serviceMapper;
    private final ServiceTypeService serviceTypeService;
    private final BookingService bookingService;

    @Autowired
    public ServiceController(AuthenticationHelper authenticationHelper, ServiceService serviceService, ServiceMapper serviceMapper, ServiceTypeService serviceTypeService, BookingService bookingService) {
        this.authenticationHelper = authenticationHelper;
        this.serviceService = serviceService;
        this.serviceMapper = serviceMapper;
        this.serviceTypeService = serviceTypeService;
        this.bookingService = bookingService;
    }

    @GetMapping("/booking/{id}")
    public List<Service> getAllServicesByBookingId(@PathVariable int id, @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Booking booking = bookingService.getById(id);
            if (!requester.isEmployee() && requester.getId() != booking.getVehicle().getOwner().getId()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorised to access the requested resources");
            }
            return serviceService.getAllByFieldMatches("booking.id", id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/service/{id}")
    public Service getServiceById(@PathVariable int id) {
        try {
            return serviceService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/booking/{bookingID}/service/{serviceTypeID}")
    public Service addServiceToBooking(@PathVariable int bookingID,
                          @PathVariable int serviceTypeID,
                          @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Booking booking = bookingService.getById(bookingID);
            ServiceType serviceTypeToAdd = serviceTypeService.getById(serviceTypeID);
            Service service = serviceMapper.createService(serviceTypeToAdd, booking);
            serviceService.create(service, requester);
            return service;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Service setPrice(@PathVariable int id,
                          @Valid @RequestBody ServiceDTO ServiceDTO,
                          @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Service serviceToUpdate = serviceService.getById(id);
            Service updatedService = serviceMapper.updateFromDTO(ServiceDTO, serviceToUpdate);
            serviceService.update(updatedService, requester);
            return updatedService;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Service deleteService(@PathVariable int id,
                          @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Service serviceToDelete = serviceService.getById(id);
            serviceService.delete(serviceToDelete, requester);
            return serviceToDelete;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}