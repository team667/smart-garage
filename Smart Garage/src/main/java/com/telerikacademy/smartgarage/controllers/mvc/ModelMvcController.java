package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.model.CreateModelDto;
import com.telerikacademy.smartgarage.models.model.ModelMapper;
import com.telerikacademy.smartgarage.models.model.UpdateModelDto;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/models")
@ApiIgnore
public class ModelMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ModelService modelService;
    private final ModelMapper modelMapper;
    private final BrandService brandService;

    @Autowired
    public ModelMvcController(AuthenticationHelper authenticationHelper, ModelService modelService, ModelMapper modelMapper, BrandService brandService) {
        this.authenticationHelper = authenticationHelper;
        this.modelService = modelService;
        this.modelMapper = modelMapper;
        this.brandService = brandService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String getAll(@RequestParam(required = false) Optional<Integer> l,
                         @RequestParam(required = false) Optional<Integer> p,
                         HttpSession httpSession,
                         Model model) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized to get this resource");
            }
            populateModuleAttributes(model, p, l);
            model.addAttribute("modelDTO", new CreateModelDto());
            return "models";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    private void populateModuleAttributes(Model model, Optional<Integer> p, Optional<Integer> l) {
        int pageSize = 0;
        int lastPage = 0;
        int currentPage = 1;
        List<com.telerikacademy.smartgarage.models.model.Model> modelList;
        if (l.isEmpty()) {
            modelList = modelService.getAll();
        } else {
            pageSize = l.get();
            lastPage = modelService.lastPageNumber(pageSize);
            if (p.isPresent()) {
                currentPage = p.get();
            }
            modelList = modelService.getAllPaginated(pageSize, currentPage);
        }
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("lastPage", lastPage);
        model.addAttribute("models", modelList);
        model.addAttribute("brands", brandService.getAll());
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id, HttpSession httpSession, Model model) {

        com.telerikacademy.smartgarage.models.model.Model modelToDisplay;
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            modelToDisplay = modelService.getById(id);
            if (!requester.isEmployee()) {
                return "redirect:/";
            }
            model.addAttribute("requester", requester);
            model.addAttribute("model", modelToDisplay);
            model.addAttribute("brands",brandService.getAll());

            UpdateModelDto updateModelDto = modelMapper.dtoFromObject(modelToDisplay);
            model.addAttribute("modelDTO", updateModelDto);
            return "model";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/create")
    public String handleVehicleCreatePage(
                                          @Valid @ModelAttribute("modelDTO") CreateModelDto createModelDto,
                                          BindingResult bindingResult,
                                          Model model,
                                          HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("requester", requester);
        if (bindingResult.hasErrors()) {
            populateModuleAttributes(model, Optional.empty(), Optional.empty());
//            model.addAttribute("modelDTO", createModelDto);
            return "models";
        }

        try {
            com.telerikacademy.smartgarage.models.model.Model model1 = modelMapper.createDtoToObject(createModelDto);
            modelService.create(model1);
            return "redirect:/models/" + model1.getId();
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "model_err", "A model already exists");
            return "models";
        }
    }

    @PostMapping("{id}/edit")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("modelDTO") UpdateModelDto modelDto,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            com.telerikacademy.smartgarage.models.model.Model modelToUpdate = modelService.getById(id);
            if (bindingResult.hasErrors()) {
                model.addAttribute("model", modelToUpdate);
                model.addAttribute("brands",brandService.getAll());
                return "model";
            }
            com.telerikacademy.smartgarage.models.model.Model updatedModel = modelMapper.updateDtoToObject(modelDto, modelToUpdate);
            modelService.update(updatedModel);
            return "redirect:/models/"+id;
        } catch (AuthenticationFailureException e){
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e){
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("{id}/delete")
    public String deleteModel(@PathVariable int id, Model model, HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);

            com.telerikacademy.smartgarage.models.model.Model modelToDelete = modelService.getById(id);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            modelService.delete(modelToDelete);
            return "redirect:/models";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

}