package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.enums.ServiceTypeSortOptions;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.servicetype.ServiceTypeDTO;
import com.telerikacademy.smartgarage.models.servicetype.ServiceTypeMapper;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/services")
public class ServiceTypeController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceTypeService serviceTypeService;
    private final ServiceTypeMapper serviceTypeMapper;

    @Autowired
    public ServiceTypeController(AuthenticationHelper authenticationHelper, ServiceTypeService serviceTypeService, ServiceTypeMapper serviceTypeMapper) {
        this.authenticationHelper = authenticationHelper;
        this.serviceTypeService = serviceTypeService;
        this.serviceTypeMapper = serviceTypeMapper;
    }

    @GetMapping
    public List<ServiceType> getAll() {
        return serviceTypeService.getAll();
    }

    @GetMapping("{id}")
    public ServiceType getById(@PathVariable int id) {
        try {
            return serviceTypeService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<ServiceType> filter(
            @RequestParam(required = false) Optional<String> name,
            @RequestParam(required = false) Optional<String> sort) {
        StringBuilder nameSb = new StringBuilder();
        StringBuilder sortSb = new StringBuilder();
        name.ifPresent(nameSb::append);
        sort.ifPresent(sortSb::append);
        return serviceTypeService.filter(
                name, Optional.of(ServiceTypeSortOptions.valueOfPreview(sortSb.toString()))
        );
    }

    @PostMapping
    public ServiceType create(@Valid @RequestBody ServiceTypeDTO ServiceTypeDTO,
                              @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            ServiceType serviceType = serviceTypeMapper.createFromDTO(ServiceTypeDTO);
            serviceTypeService.create(serviceType, requester);
            return serviceType;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorised to create services");
        }
    }

    @PutMapping("{id}")
    public ServiceType update(@PathVariable int id,
                              @Valid @RequestBody ServiceTypeDTO ServiceTypeDTO,
                              @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            ServiceType serviceTypeToUpdate = serviceTypeService.getById(id);
            ServiceType updatedServiceType = serviceTypeMapper.updateFromDTO(ServiceTypeDTO, serviceTypeToUpdate);
            serviceTypeService.update(updatedServiceType, requester);
            return updatedServiceType;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorised to update services");
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ServiceType delete(@PathVariable int id,
                              @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            ServiceType serviceTypeToDelete = serviceTypeService.getById(id);
            serviceTypeService.delete(serviceTypeToDelete, requester);
            return serviceTypeToDelete;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorised to delete services");
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}