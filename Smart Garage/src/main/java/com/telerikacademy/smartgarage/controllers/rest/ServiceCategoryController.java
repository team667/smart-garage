package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategoryDTO;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategoryMapper;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.ServiceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class ServiceCategoryController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceCategoryService serviceCategoryService;
    private final ServiceCategoryMapper serviceCategoryMapper;

    @Autowired
    public ServiceCategoryController(AuthenticationHelper authenticationHelper, ServiceCategoryService serviceCategoryService, ServiceCategoryMapper serviceCategoryMapper) {
        this.authenticationHelper = authenticationHelper;
        this.serviceCategoryService = serviceCategoryService;
        this.serviceCategoryMapper = serviceCategoryMapper;
    }

    @GetMapping
    public List<ServiceCategory> getAll() {
        return serviceCategoryService.getAll();
    }

    @GetMapping("/{id}")
    public ServiceCategory getById(@PathVariable int id) {
        try {
            return serviceCategoryService.getById(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public ServiceCategory create(@Valid @RequestBody ServiceCategoryDTO serviceCategoryDTO,
                                  @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            ServiceCategory serviceCategory = serviceCategoryMapper.createFromDTO(serviceCategoryDTO);
            serviceCategoryService.create(serviceCategory, requester);
            return serviceCategory;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorised to create categories");
        }
    }

    @PutMapping("/{id}")
    public ServiceCategory update(@PathVariable int id,
                                  @Valid @RequestBody ServiceCategoryDTO serviceCategoryDTO,
                                  @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            ServiceCategory categoryToUpdate = serviceCategoryService.getById(id);
            ServiceCategory updatedCategory = serviceCategoryMapper.updateFromDTO(serviceCategoryDTO, categoryToUpdate);
            serviceCategoryService.update(updatedCategory, requester);
            return updatedCategory;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorised to update categories");
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ServiceCategory delete(@PathVariable int id,
                                  @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            ServiceCategory categoryToDelete = serviceCategoryService.getById(id);
            serviceCategoryService.delete(categoryToDelete, requester);
            return categoryToDelete;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorised to delete categories");
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}