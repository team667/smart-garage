package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.brand.BrandMapper;
import com.telerikacademy.smartgarage.models.brand.CreateBrandDto;
import com.telerikacademy.smartgarage.models.brand.UpdateBrandDto;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.UpdateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/brands")
@ApiIgnore
public class BrandMvcController {

    private final BrandService brandService;
    private final AuthenticationHelper authenticationHelper;
    private final BrandMapper brandMapper;

    @Autowired
    public BrandMvcController(BrandService brandService, AuthenticationHelper authenticationHelper, BrandMapper brandMapper) {
        this.brandService = brandService;
        this.authenticationHelper = authenticationHelper;
        this.brandMapper = brandMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String getAll(@RequestParam(required = false) Optional<Integer> p,
                         @RequestParam(required = false) Optional<Integer> l,
                         HttpSession httpSession,
                         Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("brandDTO", new CreateBrandDto());
            populateModuleAttributes(model, p, l);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "brands";
    }

    private void populateModuleAttributes(Model model, Optional<Integer> p, Optional<Integer> l) {
        int pageSize = 0;
        int lastPage = 0;
        int currentPage = 1;
        List<Brand> brandList;
        if (l.isEmpty()) {
            brandList = brandService.getAll();
        } else {
            pageSize = l.get();
            lastPage = brandService.lastPageNumber(pageSize);
            if (p.isPresent()) {
                currentPage = p.get();
            }
            brandList = brandService.getAllPaginated(pageSize, currentPage);
        }
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("lastPage", lastPage);
        model.addAttribute("brands", brandList);
    }

    @GetMapping("{id}")
    public String getById(@PathVariable int id, HttpSession httpSession, Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Brand brand = brandService.getById(id);
            model.addAttribute("brand", brand);
            UpdateBrandDto updateBrandDto = brandMapper.dtoFromObject(brand);
            model.addAttribute("brandDTO", updateBrandDto);
            return "brand";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/create")
    public String handleCreatePage(@RequestParam(required = false) Optional<Integer> p,
                                   @RequestParam(required = false) Optional<Integer> l,
                                   @Valid @ModelAttribute("brandDTO") CreateBrandDto createBrandDto,
                                   BindingResult bindingResult,
                                   HttpSession httpSession,
                                   Model model) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized to create brands");
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("requester", requester);
        model.addAttribute("brandDTO", createBrandDto);
        populateModuleAttributes(model, p, l);
        if (bindingResult.hasErrors()) {
            return "brands";
        }

        try {
            Brand brand = brandMapper.createDtoToObject(createBrandDto);
            brandService.create(brand);
            return "redirect:/brands/" + brand.getId();
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "brand_errors", e.getMessage());
            return "brands";
        }
    }

    @PostMapping("{id}/edit")
    public String handleEditPage(@PathVariable int id,
                                 @Valid @ModelAttribute("brandDTO") UpdateBrandDto updateBrandDto,
                                 BindingResult bindingResult,
                                 HttpSession httpSession,
                                 Model model) {
        User requester;
        Brand brandToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            brandToUpdate = brandService.getById(id);
            model.addAttribute("brand", brandToUpdate);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            if (bindingResult.hasErrors()) {
                return "brand";
            }
            Brand brand = brandMapper.updateBrandFromDto(updateBrandDto, brandToUpdate);
            brandService.update(brand);
            return "redirect:/brands/" + brand.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "brand_error", e.getMessage());
            return "brand";
        }
    }

    @GetMapping("{id}/delete")
    public String deleteBrand(@PathVariable int id, HttpSession httpSession, Model model) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Brand vehicleToDelete = brandService.getById(id);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            brandService.delete(vehicleToDelete);
            return "redirect:/brands?p=1&l=10";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

}