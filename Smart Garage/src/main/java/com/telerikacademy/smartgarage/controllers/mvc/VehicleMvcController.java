package com.telerikacademy.smartgarage.controllers.mvc;


import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.enums.VehicleSortOptions;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.vehicle.*;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/vehicles")
@ApiIgnore
public class VehicleMvcController {

    private final VehicleService vehicleService;
    private final BrandService brandService;
    private final ModelService modelService;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleMapper vehicleMapper;
    private final UserService userService;

    @Autowired
    public VehicleMvcController(VehicleService vehicleService,
                                BrandService brandService,
                                ModelService modelService,
                                AuthenticationHelper authenticationHelper,
                                VehicleMapper vehicleMapper,
                                UserService userService) {
        this.vehicleService = vehicleService;
        this.brandService = brandService;
        this.modelService = modelService;
        this.authenticationHelper = authenticationHelper;
        this.vehicleMapper = vehicleMapper;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("allbrands")
    public List<Brand> popualteBrands() {
        return brandService.getAll();
    }

    @ModelAttribute("allmodels")
    public List<com.telerikacademy.smartgarage.models.model.Model> populateModels() {
        return modelService.getAll();
    }

    @ModelAttribute("owners")
    public List<User> populateUsers() {
        return userService.getAll();
    }

    @ModelAttribute("sortOptions")
    public VehicleSortOptions[] populatePostSortOptions() {
        return VehicleSortOptions.values();
    }


    @GetMapping
    public String getAll(HttpSession httpSession, Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            List<Vehicle> vehicleList;
            if (requester.isEmployee()) vehicleList = vehicleService.getAll();
            else vehicleList = vehicleService.getAllByFieldMatches("owner.id", requester.getId());

            model.addAttribute("allvehicles", vehicleList);
            model.addAttribute("vehicleDTO", new CreateVehicleDTO());
            model.addAttribute("filterDTO", new VehicleFilterDTO());
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "vehicles";
    }

    @PostMapping("/filter")
    public String filter(@Valid @ModelAttribute("filterDTO") VehicleFilterDTO vehicleFilterDTO,
                         BindingResult bindingResult,
                         HttpSession httpSession,
                         Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("vehicleDTO", new CreateVehicleDTO());
            model.addAttribute("filterDTO", new VehicleFilterDTO());
            if (bindingResult.hasErrors()) {
                return "vehicles";
            }
            var filtered = vehicleService.filter(
                    Optional.ofNullable(vehicleFilterDTO.getOwnerId() == 0 ? null : vehicleFilterDTO.getOwnerId()),
                    Optional.ofNullable(vehicleFilterDTO.getSort() == null ? null : VehicleSortOptions.valueOfPreview(vehicleFilterDTO.getSort())),
                    requester
            );
            model.addAttribute("allvehicles", filtered);
            return "vehicles";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/search")
    public String search(@Valid @ModelAttribute("filterDTO") VehicleFilterDTO vehicleFilterDTO,
                         BindingResult bindingResult,
                         HttpSession httpSession,
                         Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("vehicleDTO", new CreateVehicleDTO());
            model.addAttribute("filterDTO", new VehicleFilterDTO());
            if (bindingResult.hasErrors()) {
                return "vehicles";
            }
            List<Vehicle> filtered = new ArrayList<>();
            if (requester.isEmployee()) {
                filtered = vehicleService.adminSearch(
                        Optional.ofNullable(vehicleFilterDTO.getSearch().isBlank() ? null : vehicleFilterDTO.getSearch()));
            } else {
                filtered = vehicleService.customerSearch(
                        Optional.ofNullable(vehicleFilterDTO.getSearch().isBlank() ? null : vehicleFilterDTO.getSearch()),
                        requester.getId()
                );
            }
            model.addAttribute("allvehicles", filtered);
            return "vehicles";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id, HttpSession httpSession, Model model) {
        Vehicle vehicle;
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            vehicle = vehicleService.getById(id);
            if (!(requester.isEmployee() || vehicle.getOwner().getId() == requester.getId())) {
                return "redirect:/";
            }
            model.addAttribute("requester", requester);
            model.addAttribute("vehicle", vehicle);
            UpdateVehicleDTO updateVehicleDTO = vehicleMapper.dtoFromObject(vehicle);
            model.addAttribute("vehicleDTO", updateVehicleDTO);
            return "vehicle";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

//    @GetMapping("/create")
//    public String showVehicleCreatePage(HttpSession httpSession, Model model) {
//        model.addAttribute("users", userService.getAll());
//        try {
//            User requester = authenticationHelper.tryGetUser(httpSession);
//            model.addAttribute("requester", requester);
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        }
//        model.addAttribute("vehicleDTO", new CreateVehicleDTO());
//        return "vehicle-create";
//    }

    @PostMapping("/create")
    public String handleVehicleCreatePage(Model model,
                                          @Valid @ModelAttribute("vehicleDTO") CreateVehicleDTO createVehicleDTO,
                                          BindingResult bindingResult,
                                          HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("requester", requester);
        List<Vehicle> vehicleList;
        if (requester.isEmployee()) vehicleList = vehicleService.getAll();
        else vehicleList = vehicleService.getAllByFieldMatches("owner.id", requester.getId());

        model.addAttribute("filterDTO", new VehicleFilterDTO());
        model.addAttribute("allvehicles", vehicleList);
        model.addAttribute("users", userService.getAll());
        model.addAttribute("vehicleDTO", createVehicleDTO);
        if (bindingResult.hasErrors()) {
            return "vehicles";
        }

        try {
            Vehicle vehicle = vehicleMapper.createDTOtoObject(createVehicleDTO, requester);
            vehicleService.create(vehicle);
            return "redirect:/vehicles/" + vehicle.getId();
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vin_err", "A vehicle with vin symbols already exists");
            return "vehicles";
        }
    }

//    @GetMapping("/{id}/edit")
//    public String showEditPage(@PathVariable int id, Model model, HttpSession httpSession) {
//        User requester;
//        Vehicle vehicleToUpdate;
//        try {
//            requester = authenticationHelper.tryGetUser(httpSession);
//            vehicleToUpdate = vehicleService.getById(id);
//            if (!(requester.isEmployee() || requester.getVehicles().contains(vehicleToUpdate))) {
//                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
//            }
//            model.addAttribute("requester", requester);
//            model.addAttribute("vehicleToUpdate", vehicleToUpdate);
//            model.addAttribute("vehicleDTO", new UpdateVehicleDTO());
//            return "vehicle";
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
//            model.addAttribute("error", e.getMessage());
//            return "errors";
//        }
//    }

    @PostMapping("/{id}/edit")
    public String handleEditPage(@PathVariable int id,
                                 @Valid @ModelAttribute("vehicleDTO") UpdateVehicleDTO updateVehicleDTO,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession httpSession) {

        User requester;
        Vehicle vehicleToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            vehicleToUpdate = vehicleService.getById(id);
            if (!(requester.isEmployee() || vehicleToUpdate.getOwner().getId()==requester.getId())) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("vehicle", vehicleToUpdate);
            if (bindingResult.hasErrors()) {
                return "vehicle";
            }
            Vehicle vehicle = vehicleMapper.updateFromDto(updateVehicleDTO, vehicleToUpdate);
            vehicleService.update(vehicle);
            return "redirect:/vehicles/" + vehicle.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vin_error", "Vin is already taken");
            return "vehicle";
        }
    }

    @GetMapping("{id}/delete")
    public String deleteVehicle(@PathVariable int id, Model model, HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Vehicle vehicleToDelete = vehicleService.getById(id);
            if (!(requester.isEmployee() || vehicleToDelete.getOwner().getId() == requester.getId())) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            vehicleService.delete(vehicleToDelete);
            return "redirect:/vehicles";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }
}


