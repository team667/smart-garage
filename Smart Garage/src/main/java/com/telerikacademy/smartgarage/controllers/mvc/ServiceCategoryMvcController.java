package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategoryDTO;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategoryMapper;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.ServiceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/categories")
@ApiIgnore
public class ServiceCategoryMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceCategoryService serviceCategoryService;
    private final ServiceCategoryMapper serviceCategoryMapper;

    @Autowired
    public ServiceCategoryMvcController(AuthenticationHelper authenticationHelper,
                                        ServiceCategoryService serviceCategoryService,
                                        ServiceCategoryMapper serviceCategoryMapper) {
        this.authenticationHelper = authenticationHelper;
        this.serviceCategoryService = serviceCategoryService;
        this.serviceCategoryMapper = serviceCategoryMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllPage(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            List<ServiceCategory> categories = serviceCategoryService.getAll();
            model.addAttribute("categories", categories);
            model.addAttribute("categoryDTO", new ServiceCategoryDTO());
            return "categories";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id,
                          Model model,
                          HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access the requested resource");
            }
            ServiceCategory serviceCategory = serviceCategoryService.getById(id);
            model.addAttribute("category", serviceCategory);
            ServiceCategoryDTO categoryDTO = serviceCategoryMapper.dtoFromObject(serviceCategory);
            model.addAttribute("categoryDTO", categoryDTO);
            return "category";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

//    @GetMapping("/create")
//    public String showCreatePage(Model model, HttpSession httpSession) {
//        try {
//            User requester = authenticationHelper.tryGetUser(httpSession);
//            model.addAttribute("requester", requester);
//            model.addAttribute("categoryDTO", new ServiceCategoryDTO());
//            return "category-manager";
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        }
//    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("categoryDTO") ServiceCategoryDTO serviceCategoryDTO,
                         BindingResult bindingResult,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                model.addAttribute("categories", serviceCategoryService.getAll());
                return "categories";
            }
            ServiceCategory serviceCategory = serviceCategoryMapper.createFromDTO(serviceCategoryDTO);
            serviceCategoryService.create(serviceCategory, requester);
            return "redirect:/categories";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

//    @GetMapping("/{id}/edit")
//    public String showEditPage(@PathVariable int id,
//                               Model model,
//                               HttpSession httpSession) {
//        try {
//            User requester = authenticationHelper.tryGetUser(httpSession);
//            model.addAttribute("requester", requester);
//            ServiceCategory serviceCategoryToUpdate = serviceCategoryService.getById(id);
//            ServiceCategoryDTO serviceCategoryDTO = serviceCategoryMapper.dtoFromObject(serviceCategoryToUpdate);
//            model.addAttribute("serviceCategory", serviceCategoryDTO);
//            return "category-manager";
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
//            model.addAttribute("error", e.getMessage());
//            return "errors";
//        }
//    }

    @PostMapping("/{id}/edit")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("categoryDTO") ServiceCategoryDTO serviceCategoryDTO,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            ServiceCategory serviceCategoryToUpdate = serviceCategoryService.getById(id);
            model.addAttribute("category", serviceCategoryToUpdate);
            if (bindingResult.hasErrors()) {
                return "category";
            }
            ServiceCategory updatedCategory = serviceCategoryMapper.updateFromDTO(serviceCategoryDTO, serviceCategoryToUpdate);
            serviceCategoryService.update(updatedCategory, requester);
            return "redirect:/categories/"+id;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            ServiceCategory serviceCategoryToDelete = serviceCategoryService.getById(id);
            serviceCategoryService.delete(serviceCategoryToDelete, requester);
            return "redirect:/categories";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

}