package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.booking.CreateBookingDTO;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.models.service.ServiceDTO;
import com.telerikacademy.smartgarage.models.service.ServiceMapper;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.smartgarage.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/service")
@ApiIgnore
public class ServiceMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceService serviceService;
    private final ServiceTypeService serviceTypeService;
    private final ServiceMapper serviceMapper;

    @Autowired
    public ServiceMvcController(AuthenticationHelper authenticationHelper, ServiceService serviceService, ServiceTypeService serviceTypeService, ServiceMapper serviceMapper) {
        this.authenticationHelper = authenticationHelper;
        this.serviceService = serviceService;
        this.serviceTypeService = serviceTypeService;
        this.serviceMapper = serviceMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/{id}")
    public String showEditPage(@PathVariable int id,
                               Model model,
                               HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (!requester.isEmployee()) {
                model.addAttribute("error", "You're not authorised to access the requested resource");
                return "errors";
            }
            Service serviceToUpdate = serviceService.getById(id);
            model.addAttribute("service", serviceToUpdate);
            ServiceDTO serviceDTO = serviceMapper.dtoFromObject(serviceToUpdate);
            model.addAttribute("serviceDTO", serviceDTO);
            return "booking-service";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/{id}")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("serviceDTO") ServiceDTO serviceDTO,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Service serviceToUpdate = serviceService.getById(id);
            model.addAttribute("service", serviceToUpdate);
            model.addAttribute("serviceDTO", serviceDTO);
            if (bindingResult.hasErrors()) {
                return "booking-service";
            }
            Service updatedService = serviceMapper.updateFromDTO(serviceDTO, serviceToUpdate);
            serviceService.update(updatedService, requester);
            return "redirect:/bookings/" + updatedService.getBooking().getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

}