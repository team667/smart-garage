package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.enums.UserSortOptions;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.*;
import com.telerikacademy.smartgarage.services.contracts.UserRoleService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
@ApiIgnore
public class UsersMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserRoleService userRoleService;
    private final UserMapper userMapper;
    private final JavaMailSender javaMailSender;

    @Autowired
    public UsersMvcController(AuthenticationHelper authenticationHelper, UserService userService, UserRoleService userRoleService, UserMapper userMapper, JavaMailSender javaMailSender) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userRoleService = userRoleService;
        this.userMapper = userMapper;
        this.javaMailSender = javaMailSender;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("sortOptions")
    public UserSortOptions[] populateSortOptions() {
        return UserSortOptions.values();
    }

    @GetMapping
    public String getAll(HttpSession httpSession, Model model) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            List<User> users = userService.getAll();
            model.addAttribute("users", users);
            model.addAttribute("registerDTO", new CreateUserDTO());
            model.addAttribute("filterDTO", new UserFilterDTO());
            return "users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/filter")
    public String filter(@Valid @ModelAttribute("filterDTO") UserFilterDTO filterDTO,
                         BindingResult bindingResult,
                         HttpSession httpSession,
                         Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("registerDTO", new CreateUserDTO());
            if (bindingResult.hasErrors()) return "users";
            var filtered = userService.filter(
                    Optional.ofNullable(filterDTO.getUsername().isBlank() ? null : filterDTO.getUsername()),
                    Optional.ofNullable(filterDTO.getEmail().isBlank() ? null : filterDTO.getEmail()),
                    Optional.ofNullable(filterDTO.getPhoneNumber().isBlank() ? null : filterDTO.getPhoneNumber()),
                    Optional.ofNullable(filterDTO.getBrand().isBlank() ? null : filterDTO.getBrand()),
                    Optional.ofNullable(filterDTO.getModel().isBlank() ? null : filterDTO.getModel()),
                    Optional.ofNullable(filterDTO.getLicensePlate().isBlank() ? null : filterDTO.getLicensePlate()),
                    Optional.ofNullable(filterDTO.getStartDate() == null ? null : filterDTO.getStartDate()),
                    Optional.ofNullable(filterDTO.getEndDate() == null ? null : filterDTO.getEndDate()),
                    Optional.ofNullable(filterDTO.getSort() == null ? null : UserSortOptions.valueOfPreview(filterDTO.getSort())),
                    requester);
            model.addAttribute("users", filtered);
            return "users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("errors", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/search")
    public String search(@Valid @ModelAttribute("filterDTO") UserFilterDTO filterDTO,
                         BindingResult bindingResult,
                         HttpSession httpSession,
                         Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("registerDTO", new CreateUserDTO());
            if (bindingResult.hasErrors()) return "users";
            var filtered = userService.search(
                    Optional.ofNullable(filterDTO.getSearch().isBlank() ? null : filterDTO.getSearch()), requester);
            model.addAttribute("users", filtered);
            return "users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("errors", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, HttpSession httpSession, Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            User userToGet = userService.getById(id);
            if (!requester.isEmployee() && requester.getId() != userToGet.getId()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("requester", requester);
            model.addAttribute("user", userToGet);
            UpdateUserDTO updateUserDTO = userMapper.dtoFromObject(userToGet);
            model.addAttribute("userDTO", updateUserDTO);
            model.addAttribute("roles", userRoleService.getAll());
            return "user";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

//    @GetMapping("{id}/edit")
//    public String showEditpage(@PathVariable int id, HttpSession httpSession, Model model) {
//        User requester;
//        User userToUpdate;
//        try {
//            requester = authenticationHelper.tryGetUser(httpSession);
//            userToUpdate = userService.getById(id);
//            if (!requester.isEmployee() || requester.getId() != userToUpdate.getId()) {
//                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
//            }
//            model.addAttribute("requester", requester);
//            model.addAttribute("userToUpdate", userToUpdate);
//            model.addAttribute("editprofile", new UpdateUserDTO());
//            return "user-edit";
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
//            model.addAttribute("error", e.getMessage());
//            return "errors";
//        }
//    }

    @PostMapping("{id}/edit")
    public String handleEditPage(@PathVariable int id,
                                 @Valid @ModelAttribute("userDTO") UpdateUserDTO updateUserDTO,
                                 BindingResult bindingResult,
                                 HttpSession httpSession,
                                 Model model) {
        User requester;
        User userToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            userToUpdate = userService.getById(id);
            if (!requester.isEmployee() && requester.getId() != userToUpdate.getId()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("user", userToUpdate);
            model.addAttribute("roles", userRoleService.getAll());
            if (bindingResult.hasErrors()) {
                return "user";
            }
            if ((updateUserDTO.getPassword() != null && updateUserDTO.getConfirmPassword() != null) &&
                    !updateUserDTO.getPassword().equals(updateUserDTO.getConfirmPassword())) {
                bindingResult.rejectValue("confirmPassword", "password_err", "Passwords must match");
                return "user";
            }
            User user = userMapper.updateDtoToObject(updateUserDTO, userToUpdate);
            userService.update(user, requester);
            return "redirect:/users/" + user.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "email_err", "Email is already taken");
            return "user";
        }
    }


    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id,
                             Model model,
                             HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            User userToDelete = userService.getById(id);
            System.out.println(requester.isEmployee());
            if (!requester.isEmployee() && requester.getId() != userToDelete.getId()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource buddy");
            }
            userService.delete(userToDelete, requester);
            if (!requester.isEmployee()) httpSession.removeAttribute("currentUser");
            return "redirect:/users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }


}