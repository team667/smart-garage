package com.telerikacademy.smartgarage.controllers.rest;

import com.mysql.cj.xdevapi.JsonParser;
import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.UpdateUserDTO;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.CreateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.UpdateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.models.vehicle.VehicleMapper;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final AuthenticationHelper authenticationHelper;
    private final VehicleService vehicleService;
    private final VehicleMapper vehicleMapper;

    public VehicleController(AuthenticationHelper authenticationHelper, VehicleService vehicleService, VehicleMapper vehicleMapper) {
        this.authenticationHelper = authenticationHelper;
        this.vehicleService = vehicleService;
        this.vehicleMapper = vehicleMapper;
    }

    @GetMapping
    public List<Vehicle> getAll(@RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized");
            }
            return vehicleService.getAll();
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

//    @GetMapping(value = "/makes")
//    public void fetchApi(Model model) throws IOException, InterruptedException {
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create("https://the-vehicles-api.herokuapp.com/brands/"))
//                .header("content-type", "application/json")
//                .method("GET", HttpRequest.BodyPublishers.noBody())
//                .build();
//        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
//        JSONArray jsonArray = new JSONArray(response.body());
//
//        for (int i = 0; i <  jsonArray.length(); i++) {
//            JSONObject brand0 = (JSONObject) jsonArray.get(i);
//            System.out.println(brand0.get("brand").toString());
//        }
//
//    }

//    @GetMapping(value = "/fetchModels")
//    public List<String> fetchModels() throws IOException, InterruptedException {
//        for (int i = 1; i < 125; i++) {
//            HttpRequest request = HttpRequest.newBuilder()
//                    .uri(URI.create("https://the-vehicles-api.herokuapp.com/models?brandId="+i))
//                    .header("content-type", "application/json")
//                    .method("GET", HttpRequest.BodyPublishers.noBody())
//                    .build();
//            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
//
//
//        }
//
////    System.out.println(response.body());
////    return List.of(response.body());
//
//
//    }


    @GetMapping("/{id}")
    public Vehicle getById(@PathVariable int id, @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Vehicle vehicleToGet = vehicleService.getById(id);
            if (!(requester.isEmployee() || vehicleToGet.getOwner().getId() == requester.getId())) {
                throw new UnauthorisedOperationException("You are neither employee nor owner ");
            }
            return vehicleToGet;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/user/{id}")
    public List<Vehicle> getByUserId(@PathVariable int id) {
        return vehicleService.getByUserId(id);
    }

    @PostMapping
    public Vehicle createVehicle(@Valid @RequestBody CreateVehicleDTO createVehicleDTO,
                                 @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Vehicle vehicle = vehicleMapper.createDTOtoObject(createVehicleDTO, requester);
            vehicleService.create(vehicle);
            return vehicle;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Vehicle update(@PathVariable int id,
                          @Valid @RequestBody UpdateVehicleDTO updateVehicleDTO,
                          @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Vehicle vehicleToUpdate = vehicleService.getById(id);
            Vehicle updateVehicle = vehicleMapper.updateFromDto(updateVehicleDTO, vehicleToUpdate);
            vehicleService.update(updateVehicle);
            return updateVehicle;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Vehicle delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User requester = authenticationHelper.tryGetUser(headers);
            Vehicle vehicleToDelete = vehicleService.getById(id);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized to delete user");
            }
            vehicleService.delete(vehicleToDelete);
            return vehicleToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
