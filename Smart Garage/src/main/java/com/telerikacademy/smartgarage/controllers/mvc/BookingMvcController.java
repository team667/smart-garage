package com.telerikacademy.smartgarage.controllers.mvc;

import com.lowagie.text.DocumentException;
import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.enums.Currency;
import com.telerikacademy.smartgarage.config.CurrencyApiConfig;
import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.enums.BookingSortOptions;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.booking.*;
import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.models.service.ServiceMapper;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.BookingService;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.smartgarage.services.contracts.ServiceTypeService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/bookings")
@ApiIgnore
public class BookingMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final BookingService bookingService;
    private final BookingMapper bookingMapper;
    private final ServiceTypeService serviceTypeService;
    private final ServiceService serviceService;
    private final ServiceMapper serviceMapper;
    private final CurrencyConverter currencyConverter;
    private final UserService userService;

    @Autowired
    public BookingMvcController(BookingService bookingService, AuthenticationHelper authenticationHelper, BookingMapper bookingMapper, ServiceTypeService service, ServiceTypeService serviceTypeService, ServiceService serviceService, ServiceMapper serviceMapper, CurrencyApiConfig currencyApiConfig, CurrencyConverter currencyConverter, UserService userService) {
        this.bookingService = bookingService;
        this.authenticationHelper = authenticationHelper;
        this.bookingMapper = bookingMapper;
        this.serviceTypeService = serviceTypeService;
        this.serviceService = serviceService;
        this.serviceMapper = serviceMapper;
        this.currencyConverter = currencyConverter;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isModifyingBooking")
    public boolean populateIsModifyingBooking(HttpSession httpSession) {
        return httpSession.getAttribute("bookingId") != null;
    }

    @ModelAttribute("sortOptions")
    public BookingSortOptions[] populateSortOptions() {
        return BookingSortOptions.values();
    }

    @GetMapping
    public String showAllBookings(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            populateModelAttributes(model,requester);
            model.addAttribute("users", userService.getAll());
            model.addAttribute("bookingDTO", new CreateBookingDTO());
            return "bookings";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/filter")
    public String filter(@Valid @ModelAttribute("filterDTO") BookingFilterDTO bookingFilterDTO,
                         BindingResult bindingResult,
                         HttpSession httpSession,
                         Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("users", userService.getAll());
            model.addAttribute("requester", requester);
            model.addAttribute("bookingDTO", new CreateBookingDTO());
            if (bindingResult.hasErrors()) return "bookings";
            var filtered = bookingService.filter(
                    Optional.ofNullable(bookingFilterDTO.getUserId() == 0 ? null : bookingFilterDTO.getUserId()),
                    Optional.ofNullable(bookingFilterDTO.getModelId() == 0 ? null : bookingFilterDTO.getModelId()),
                    Optional.ofNullable(bookingFilterDTO.getDate() == null ? null : bookingFilterDTO.getDate()),
                    Optional.ofNullable(bookingFilterDTO.getSort() == null ? null : BookingSortOptions.valueOfPreview(bookingFilterDTO.getSort())),
                    requester);
            model.addAttribute("bookings", filtered);
            return "bookings";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String showBooking(@PathVariable int id,
                              Model model,
                              HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Booking booking = bookingService.getById(id);
            model.addAttribute("booking", booking);
            if (!requester.isEmployee() && requester.getId() != booking.getVehicle().getOwner().getId()) {
                model.addAttribute("error", "You're not authorised to access this resource");
                return "errors";
            }

            UpdateBookingDTO updateBookingDTO = bookingMapper.dtoFromObject(booking);
            model.addAttribute("bookingDTO", updateBookingDTO);

            model.addAttribute("exportDTO", new BookingExportDTO());
            Currency[] currencies = Currency.values();
            model.addAttribute("currencies", currencies);
            return "booking";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/create")
    public String create(Model model,
                         @Valid @ModelAttribute("bookingDTO") CreateBookingDTO createBookingDTO,
                         BindingResult bindingResult,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("users", userService.getAll());
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                populateModelAttributes(model, requester);
                return "bookings";
            }
            Booking booking = bookingMapper.createFromDTO(createBookingDTO);
            bookingService.create(booking);
            httpSession.setAttribute("bookingId", booking.getId());
            return "redirect:/bookings/" + booking.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    private void populateModelAttributes(Model model, User requester) {
        List<Booking> bookings;
        if (requester.isEmployee()) bookings = bookingService.getAll();
        else bookings = bookingService.getAllByFieldMatches("vehicle.owner.id", requester.getId());
        model.addAttribute("bookings", bookings);
        String dateFormat = "d MMMM yyyy";
        model.addAttribute("dateFormat", dateFormat);
        String yearFormat = "yyyy";
        model.addAttribute("yearFormat", yearFormat);
        model.addAttribute("filterDTO", new BookingFilterDTO());
    }

    @PostMapping("/{id}/edit")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("bookingDTO") UpdateBookingDTO updateBookingDTO,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Booking bookingToUpdate = bookingService.getById(id);
            if (bindingResult.hasErrors()) {
                model.addAttribute("booking", bookingToUpdate);
                model.addAttribute("exportDTO", new BookingExportDTO());
                Currency[] currencies = Currency.values();
                model.addAttribute("currencies", currencies);
                model.addAttribute("bookingDTO", updateBookingDTO);
                return "booking";
            }
            Booking updatedBooking = bookingMapper.updateFromDTO(updateBookingDTO, bookingToUpdate);
            bookingService.update(updatedBooking, requester);
            return "redirect:/bookings/" + id;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            Booking bookingToDelete = bookingService.getById(id);
            if (httpSession.getAttribute("bookingId") != null) {
                httpSession.removeAttribute("bookingId");
            }
            bookingService.delete(bookingToDelete, requester);
            return "redirect:/bookings";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/{id}/add")
    public String showAddServicesPage(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Booking booking = bookingService.getById(id);
            List<ServiceType> bookingServices = new ArrayList<>();
            for (Service service : booking.getServices()) {
                bookingServices.add(service.getServiceType());
            }
            httpSession.setAttribute("bookingServices", bookingServices);
            httpSession.setAttribute("bookingId", id);
            return "redirect:/services?p=1&l=10";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/add/{id}")
    public String addService(@PathVariable int id,
                             HttpSession httpSession,
                             Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            ServiceType serviceType = serviceTypeService.getById(id);
            int bookingId = (int) httpSession.getAttribute("bookingId");
            Booking booking = bookingService.getById(bookingId);
            Service service = serviceMapper.createService(serviceType, booking);
            serviceService.create(service, requester);
            httpSession.removeAttribute("bookingId");
            return "redirect:/bookings/" + booking.getId();
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/remove/{id}")
    public String removeService(@PathVariable int id,
                                HttpSession httpSession,
                                Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            Service serviceToRemove = serviceService.getById(id);
            serviceService.delete(serviceToRemove, requester);
            return "redirect:/bookings/" + serviceToRemove.getBooking().getId();
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/{id}/export/pdf")
    public void exportToPDF(@PathVariable int id,
                            @Valid @ModelAttribute("exportDTO") BookingExportDTO bookingExportDTO,
                            BindingResult bindingResult,
                            HttpServletResponse response,
                            HttpSession httpSession,
                            Model model) throws DocumentException, IOException {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            Booking booking = bookingService.getById(id);
            if (booking.getCustomer().getId() != requester.getId() && !requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access this resource");
            }
            if (bindingResult.hasErrors()) {
                response.sendRedirect("/bookings/" + id);
            }
            response.setContentType("application/pdf");
            DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            String currentDateTime = dateFormatter.format(new Date());

            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=booking_" + currentDateTime + ".pdf";
            response.setHeader(headerKey, headerValue);

            Double rate;
            if (bookingExportDTO.getCurrency() == Currency.BGN) rate = 1.0;
            else rate = currencyConverter.rate(Currency.BGN, bookingExportDTO.getCurrency());

            BookingPdfExporter exporter = new BookingPdfExporter(booking, bookingExportDTO, rate);
            exporter.export(response);
        } catch (AuthenticationFailureException e) {
            response.sendRedirect("/auth/login");
        } catch (UnauthorisedOperationException e) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println(e.getMessage());
            out.close();
        }
    }

}