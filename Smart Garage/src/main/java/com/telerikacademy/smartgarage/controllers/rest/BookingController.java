package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.booking.BookingMapper;
import com.telerikacademy.smartgarage.models.booking.CreateBookingDTO;
import com.telerikacademy.smartgarage.models.booking.UpdateBookingDTO;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/bookings")
public class BookingController {

    private final String AUTH_ERROR = "You're not authorised to access the requested resources";

    private final AuthenticationHelper authenticationHelper;
    private final BookingService bookingService;
    private final BookingMapper bookingMapper;

    @Autowired
    public BookingController(AuthenticationHelper authenticationHelper, BookingService bookingService, BookingMapper bookingMapper) {
        this.authenticationHelper = authenticationHelper;
        this.bookingService = bookingService;
        this.bookingMapper = bookingMapper;
    }

    @GetMapping
    public List<Booking> getAll(@RequestHeader HttpHeaders httpHeaders) {
        User requester = authenticationHelper.tryGetUser(httpHeaders);
        if (!requester.isEmployee()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTH_ERROR);
        }
        return bookingService.getAll();
    }

    @GetMapping("/{id}")
    public Booking getById(@PathVariable int id, @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Booking booking = bookingService.getById(id);
            if (!requester.isEmployee() && requester.getId() != booking.getVehicle().getOwner().getId()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTH_ERROR);
            }
            return booking;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/user/{id}")
    public List<Booking> getByUserId(@PathVariable int id, @RequestHeader HttpHeaders httpHeaders) {
        User requester = authenticationHelper.tryGetUser(httpHeaders);
        if (!requester.isEmployee() && requester.getId() != id) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTH_ERROR);
        }
        return bookingService.getAllByFieldMatches("vehicle.owner.id", id);
    }

    @PostMapping
    public Booking create(@Valid @RequestBody CreateBookingDTO createBookingDTO,
                          @RequestHeader HttpHeaders httpHeaders) {
        authenticationHelper.tryGetUser(httpHeaders);
        Booking booking = bookingMapper.createFromDTO(createBookingDTO);
        bookingService.create(booking);
        return booking;
    }

    @PutMapping("/{id}")
    public Booking update(@PathVariable int id,
                          @Valid @RequestBody UpdateBookingDTO updateBookingDTO,
                          @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Booking bookingToUpdate = bookingService.getById(id);
            Booking updatedBooking = bookingMapper.updateFromDTO(updateBookingDTO, bookingToUpdate);
            bookingService.update(updatedBooking, requester);
            return updatedBooking;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Booking delete(@PathVariable int id,
                          @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Booking booking = bookingService.getById(id);
            bookingService.delete(booking, requester);
            return booking;
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTH_ERROR);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}