package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.models.user.CreateUserDTO;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.user.UserFilterDTO;
import com.telerikacademy.smartgarage.models.user.UserMapper;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
@RequestMapping("/register")
@ApiIgnore
public class RegistrationController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserMapper userMapper;
    private final JavaMailSender javaMailSender;

    @Autowired
    public RegistrationController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper, JavaMailSender javaMailSender) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.javaMailSender = javaMailSender;
    }

//    @GetMapping
//    public String getRegisterPage(Model model) {
//        model.addAttribute("registerDTO", new CreateUserDTO());
//        return "registration";
//    }

    @PostMapping
    public String handleRegisterPage(HttpSession httpSession,
                                     Model model,
                                     @Valid @ModelAttribute("registerDTO") CreateUserDTO createUserDTO,
                                     BindingResult bindingResult) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("filterDTO", new UserFilterDTO());
        model.addAttribute("users", userService.getAll());

        if (bindingResult.hasErrors()) {
            return "users";
        }

        try {
            User user = userMapper.createDtoToObject(createUserDTO);
            userService.create(user);
            sendEmail(user.getEmail(), user.getUsername(), user.getPassword());
            return "redirect:/users";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("username and email")) {
                bindingResult.rejectValue("username", "username-err", "Username already exists");
                bindingResult.rejectValue("email", "email-err", "User with this email already exists");
                return "users";
            } else if (e.getMessage().contains("username")) {
                bindingResult.rejectValue("username", "username-err", "Username already exists");
                return "users";
            } else if (e.getMessage().contains("phone")) {
                bindingResult.rejectValue("phoneNumber", "phone-err", e.getMessage());
                return "users";
            } else {
                bindingResult.rejectValue("email", "email-err", "User with this email already exists");
                return "users";
            }
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending email");
            return "errors";
        }
    }

    public void sendEmail(String recipientEmail, String username, String password)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("no-reply@62.techteamsupportandinfo.com", "Tech Support");
        helper.setTo(recipientEmail);

        String subject = "Smart Garage - Welcome on board!";

        String content = "<p>Hello,</p>"
                + "<p>Account details: </p>"
                + "<p>Username: " + username + "</p>"
                + "<p>Password: " + password + "</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        javaMailSender.send(message);
    }

}