package com.telerikacademy.smartgarage.exceptions;

public class UnauthorisedOperationException extends RuntimeException {

    public UnauthorisedOperationException(String message) {
        super(message);
    }

}