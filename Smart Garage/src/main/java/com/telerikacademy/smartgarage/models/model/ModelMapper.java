package com.telerikacademy.smartgarage.models.model;

import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.vehicle.UpdateVehicleDTO;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ModelMapper {

    private final ModelService modelService;
    private final BrandService brandService;

    @Autowired
    public ModelMapper(ModelService modelService, BrandService brandService) {
        this.modelService = modelService;
        this.brandService = brandService;
    }

    public Model createDtoToObject(CreateModelDto createModelDto) {
        Model model = new Model();
        Brand brand = brandService.getById(createModelDto.getBrandId());
        model.setBrand(brand);
        model.setName(createModelDto.getName());
        return model;
    }

    public Model thePopulator(FetchModelDto fetchModelDto) {
        Brand brand = brandService.getByName(fetchModelDto.getBrandName());
        Model model = new Model();
        model.setBrand(brand);
        model.setName(fetchModelDto.getModelName());
        return model;
    }

    public Model updateDtoToObject(UpdateModelDto updateModelDto, Model modelToUpdate) {
        Model model = new Model();
        model.setId(modelToUpdate.getId());
        Brand brand = brandService.getById(updateModelDto.getBrandId());
        model.setBrand(brand);

        if (updateModelDto.getName() != null) model.setName(updateModelDto.getName());
        else model.setName(modelToUpdate.getName());

        return model;
    }

    public UpdateModelDto dtoFromObject(Model model) {
        UpdateModelDto updateModelDto = new UpdateModelDto();
        updateModelDto.setBrandId(model.getBrand().getId());
        updateModelDto.setName(model.getName());
        return updateModelDto;
    }

}