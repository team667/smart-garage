package com.telerikacademy.smartgarage.models.vehicle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telerikacademy.smartgarage.utils.validators.constraints.LicensePlateNumber;
import com.telerikacademy.smartgarage.utils.validators.constraints.Year;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

public class UpdateVehicleDTO {

    @Nullable
    @Positive(message = "Please select a brand")
    private int brand_id;

    @Nullable
    @Positive(message = "Please select a model")
    private int model_id;

    @Nullable
    @LicensePlateNumber
    private String licence_plate;

    @Nullable
    private String vin;

    @Nullable
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Year
    private LocalDate year_Of_Creation;


    public UpdateVehicleDTO(int brand_id, int model_id, @Nullable String licence_plate, @Nullable String vin) {
        this.brand_id = brand_id;
        this.model_id = model_id;
        this.licence_plate = licence_plate;
        this.vin = vin;
    }

    public UpdateVehicleDTO() {
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getModel_id() {
        return model_id;
    }

    public void setModel_id(int model_id) {
        this.model_id = model_id;
    }

    public String getLicence_plate() {
        return licence_plate;
    }

    public void setLicence_plate( String licence_plate) {
        this.licence_plate = licence_plate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin( String vin) {
        this.vin = vin;
    }

    @Nullable
    public LocalDate getYear_Of_Creation() {
        return year_Of_Creation;
    }

    public void setYear_Of_Creation(@Nullable LocalDate year_Of_Creation) {
        this.year_Of_Creation = year_Of_Creation;
    }
}
