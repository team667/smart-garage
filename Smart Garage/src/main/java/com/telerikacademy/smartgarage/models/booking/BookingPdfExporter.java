package com.telerikacademy.smartgarage.models.booking;

import com.lowagie.text.Font;
import com.lowagie.text.*;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.telerikacademy.smartgarage.models.service.Service;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.List;

public class BookingPdfExporter {

    private final Booking booking;
    private final BookingExportDTO bookingExportDTO;
    private final Double rate;

    public BookingPdfExporter(Booking booking, BookingExportDTO bookingExportDTO, Double rate) {
        this.booking = booking;
        this.bookingExportDTO = bookingExportDTO;
        this.rate = rate;
    }

    private void writeTableHeader(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
//        cell.setBackgroundColor(Color.BLUE);
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.BLACK);

        Font fontBold = FontFactory.getFont(FontFactory.HELVETICA);
        fontBold.setColor(Color.BLACK);
        fontBold.setStyle(Font.BOLD);

        cell.setPhrase(new Phrase("Booking ID:", fontBold));
        table.addCell(cell);

        cell.setPhrase(new Phrase("User details:", fontBold));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Vehicle details:", fontBold));
        table.addCell(cell);
    }

    private void writeUserData(PdfPTable table) {
        PdfPCell firstCell = new PdfPCell(new Phrase(String.valueOf(booking.getId())));
        firstCell.setBorder(Rectangle.NO_BORDER);
        firstCell.setRowspan(3);
        PdfPCell otherCell = new PdfPCell();
        otherCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(firstCell);
        otherCell.setPhrase(new Phrase(booking.getCustomer().getUsername()));
        table.addCell(otherCell);
        otherCell.setPhrase(new Phrase(booking.getVehicle().getBrand().getName()));
        table.addCell(otherCell);
        otherCell.setPhrase(new Phrase(booking.getCustomer().getEmail()));
        table.addCell(otherCell);
        otherCell.setPhrase(new Phrase(booking.getVehicle().getModel().getName()));
        table.addCell(otherCell);
        otherCell.setPhrase(new Phrase(booking.getCustomer().getPhoneNumber()));
        table.addCell(otherCell);
        otherCell.setPhrase(new Phrase(booking.getVehicle().getLicensePlate()));
        table.addCell(otherCell);
    }

    private void writeServicesData(PdfPTable table) {
        PdfPCell borderlessCell = new PdfPCell();
        borderlessCell.setBorder(Rectangle.NO_BORDER);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.BLACK);

        Font fontBold = FontFactory.getFont(FontFactory.HELVETICA);
        fontBold.setColor(Color.BLACK);
        fontBold.setStyle(Font.BOLD);

        List<Service> serviceList = booking.getServices();

        borderlessCell.setPhrase(new Phrase("Service name:", fontBold));
        table.addCell(borderlessCell);
        borderlessCell.setPhrase(new Phrase("Price:", fontBold));
        table.addCell(borderlessCell);

        double totalCost = 0;
        for (Service service : serviceList) {
            totalCost += service.getPrice();
            borderlessCell.setPhrase(new Phrase(service.getServiceType().getName()));
            table.addCell(borderlessCell);
            borderlessCell.setPhrase(new Phrase(String.valueOf(service.getPrice())));
            table.addCell(borderlessCell);
        }
        borderlessCell.setPhrase(new Phrase());
        table.addCell(borderlessCell);
        table.addCell(borderlessCell);
        table.addCell(borderlessCell);
        totalCost *= rate;
        borderlessCell.setPhrase(new Phrase("Total: " +
                bookingExportDTO.getCurrency().toString() + " " +
                String.format("%.2f",totalCost),
                fontBold));
        table.addCell(borderlessCell);
    }

    public void export(HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());


        document.open();

        Chunk line = new Chunk(new LineSeparator());

        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLUE);

        Paragraph p = new Paragraph("Smart Garage", font);
        p.setAlignment(Paragraph.ALIGN_LEFT);

        document.add(p);
        document.add(line);

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(100f);
        table.setWidths(new float[]{1.5f, 3.5f, 3.0f});

        writeTableHeader(table);
        writeUserData(table);

        PdfPTable table2 = new PdfPTable(2);
        table2.setWidthPercentage(100f);

        writeServicesData(table2);

        document.add(table);
        document.add(line);

        document.add(table2);

        document.close();
    }

}