package com.telerikacademy.smartgarage.models.booking;

import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_id")
    int id;

    @OneToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @Transient
    private User customer;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "date")
    LocalDate dropOffDate;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "booking", cascade = CascadeType.ALL)
    private List<Service> services;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public User getCustomer() {
        return vehicle.getOwner();
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public LocalDate getDropOffDate() {
        return dropOffDate;
    }

    public void setDropOffDate(LocalDate dateTime) {
        this.dropOffDate = dateTime;
    }

    public double getTotalCost() {
        double totalCost = 0;
        if (services != null && !services.isEmpty()) {
            for (Service service : services) {
                totalCost += service.getPrice();
            }
        }
        return totalCost;
    }

}