package com.telerikacademy.smartgarage.models.brand;

import com.telerikacademy.smartgarage.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BrandMapper {

    private final BrandService brandService;

    @Autowired
    public BrandMapper(BrandService brandService) {
        this.brandService = brandService;
    }

    public Brand createDtoToObject(CreateBrandDto createBrandDto){
        Brand brand = new Brand();
        brand.setName(createBrandDto.getName());

        return brand;
    }

    public Brand updateBrandFromDto(UpdateBrandDto updateBrandDto,Brand brandToUpdate){
        Brand brand = new Brand();
        brand.setId(brandToUpdate.getId());
        brand.setName(updateBrandDto.getName());

        return brand;
    }

    public UpdateBrandDto dtoFromObject(Brand brand){
        UpdateBrandDto updateBrandDto = new UpdateBrandDto();
        updateBrandDto.setName(brand.getName());
        return updateBrandDto;
    }

}