package com.telerikacademy.smartgarage.models.vehicle;

import com.telerikacademy.smartgarage.utils.validators.constraints.LicensePlateNumber;
import com.telerikacademy.smartgarage.utils.validators.constraints.Year;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class CreateVehicleDTO {

    public Integer userId;

    @NotNull
    @Positive(message = "Please select brand")
    public int brandId;

    @NotNull
    @Positive(message = "Please select model")
    public int  modelId;

    @NotNull
    @Size(min = 17,max = 17,message = "Vin should be 17 symbols")
    public String vin;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Year
    public LocalDate yearOfCreation;

    @NotNull
    @LicensePlateNumber
    public String licence_plate;

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public LocalDate getYearOfCreation() {
        return yearOfCreation;
    }

    public void setYearOfCreation(LocalDate yearOfCreation) {
        this.yearOfCreation = yearOfCreation;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLicence_plate() {
        return licence_plate;
    }

    public void setLicence_plate(String licence_plate) {
        this.licence_plate = licence_plate;
    }

}