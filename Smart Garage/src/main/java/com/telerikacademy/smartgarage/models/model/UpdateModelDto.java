package com.telerikacademy.smartgarage.models.model;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UpdateModelDto {

    @Nullable
    @Size(min = 1, message = "Model name cannot be empty")
    private String name;

    private Integer brandId;

    public UpdateModelDto(@Nullable String name) {
        this.name = name;
    }

    public UpdateModelDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        if (name.isEmpty()) name = null;
        this.name = name;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

}