package com.telerikacademy.smartgarage.models.service;

import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import org.springframework.stereotype.Component;

@Component
public class ServiceMapper {

    public Service createService(ServiceType serviceType, Booking booking){
        Service service = new Service();
        service.setServiceType(serviceType);
        service.setBooking(booking);
        return service;
    }

    public Service updateFromDTO(ServiceDTO serviceDTO, Service serviceToUpdate) {
        serviceToUpdate.setPrice(serviceDTO.getPrice());
        return serviceToUpdate;
    }

    public ServiceDTO dtoFromObject(Service service) {
        ServiceDTO serviceDTO = new ServiceDTO();
        serviceDTO.setServiceType(service.getServiceType());
        serviceDTO.setPrice(service.getPrice());
        return serviceDTO;
    }

}