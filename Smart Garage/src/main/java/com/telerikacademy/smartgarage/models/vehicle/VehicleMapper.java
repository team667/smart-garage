package com.telerikacademy.smartgarage.models.vehicle;

import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Year;

@Component
public class VehicleMapper {

    private final VehicleService vehicleService;
    private final BrandService brandService;
    private final ModelService modelService;
    private final UserService userService;

    @Autowired
    public VehicleMapper(VehicleService vehicleService, BrandService brandService, ModelService modelService, UserService userService) {
        this.vehicleService = vehicleService;
        this.brandService = brandService;
        this.modelService = modelService;
        this.userService = userService;
    }

    public Vehicle createDTOtoObject(CreateVehicleDTO createVehicleDTO, User requester) {
        Vehicle vehicle = new Vehicle();
//        Brand brand = brandService.getById(createVehicleDTO.getBrandId());
        Model model = modelService.getById(createVehicleDTO.getModelId());
        Year year = Year.of(createVehicleDTO.getYearOfCreation().getYear());

//        vehicle.setBrand(brand);
        vehicle.setModel(model);
        vehicle.setBrand(model.getBrand());
        vehicle.setVin(createVehicleDTO.getVin());
        vehicle.setLicensePlate(createVehicleDTO.getLicence_plate().toUpperCase());
        if (createVehicleDTO.getUserId() == null) vehicle.setOwner(requester);
        else vehicle.setOwner(userService.getById(createVehicleDTO.getUserId()));
        vehicle.setYearOfCreation(createVehicleDTO.getYearOfCreation());
//        vehicle.setYearOfCreation(LocalDate.from(year));

        return vehicle;
    }

    public Vehicle updateFromDto(UpdateVehicleDTO updateVehicleDTO, Vehicle vehicletoUpdate) {
        Vehicle vehicle = new Vehicle();
        vehicle.setId(vehicletoUpdate.getId());

//        Brand brand = brandService.getById(updateVehicleDTO.getBrand_id());
//        if(updateVehicleDTO.getBrand_id() != 0) vehicle.setBrand(brand);
//        else vehicle.setBrand(vehicletoUpdate.getBrand());

        Model model = modelService.getById(updateVehicleDTO.getModel_id());
        if (updateVehicleDTO.getModel_id() != 0) vehicle.setModel(model);
        else vehicle.setModel(vehicletoUpdate.getModel());

        vehicle.setOwner(vehicletoUpdate.getOwner());

        vehicle.setYearOfCreation(vehicletoUpdate.getYearOfCreation());

        if (updateVehicleDTO.getLicence_plate() != null) vehicle.setLicensePlate(updateVehicleDTO.getLicence_plate());
        else vehicle.setLicensePlate(vehicletoUpdate.getLicensePlate());

        if (updateVehicleDTO.getVin() != null) vehicle.setVin(updateVehicleDTO.getVin());
        else vehicle.setVin(vehicletoUpdate.getVin());

        return vehicle;
    }

    public UpdateVehicleDTO dtoFromObject(Vehicle vehicle){
        UpdateVehicleDTO updateVehicleDTO = new UpdateVehicleDTO();
        updateVehicleDTO.setBrand_id(vehicle.getBrand().getId());
        updateVehicleDTO.setModel_id(vehicle.getModel().getId());
        updateVehicleDTO.setLicence_plate(vehicle.getLicensePlate());
        updateVehicleDTO.setVin(vehicle.getVin());
        updateVehicleDTO.setYear_Of_Creation(vehicle.getYearOfCreation());
        return updateVehicleDTO;
    }

}