package com.telerikacademy.smartgarage.models.booking;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.utils.validators.constraints.Booking;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.List;

public class UpdateBookingDTO {

    @Positive(message = "Please select a vehicle")
    private int vehicleId;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Booking
    private LocalDate dateTime;

    private List<Service> services;

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public LocalDate getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDate dateTime) {
        this.dateTime = dateTime;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

}