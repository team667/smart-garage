package com.telerikacademy.smartgarage.models.model;

import javax.validation.constraints.NotNull;

public class FetchModelDto {

    public String brandName;

    public String modelName;


    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
