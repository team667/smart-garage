package com.telerikacademy.smartgarage.models.booking;

import com.posadskiy.currencyconverter.enums.Currency;

import javax.validation.constraints.NotNull;

public class BookingExportDTO {

    @NotNull
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

}