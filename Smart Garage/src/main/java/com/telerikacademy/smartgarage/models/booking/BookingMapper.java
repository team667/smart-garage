package com.telerikacademy.smartgarage.models.booking;

import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookingMapper {

    private final VehicleService vehicleService;

    @Autowired
    public BookingMapper(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    public Booking createFromDTO(CreateBookingDTO createBookingDTO) {
        Booking booking = new Booking();
//        booking.setServices(createBookingDTO.getServices());
        Vehicle vehicle = vehicleService.getById(createBookingDTO.getVehicleId());
        booking.setVehicle(vehicle);
        return booking;
    }

    public Booking updateFromDTO(UpdateBookingDTO updateBookingDTO, Booking bookingToUpdate) {
        if (updateBookingDTO.getServices() != null) bookingToUpdate.setServices(updateBookingDTO.getServices());
        if (updateBookingDTO.getVehicleId() != 0){
            Vehicle vehicle = vehicleService.getById(updateBookingDTO.getVehicleId());
            bookingToUpdate.setVehicle(vehicle);
        }
        if (updateBookingDTO.getDateTime() != null) bookingToUpdate.setDropOffDate(updateBookingDTO.getDateTime());
        return bookingToUpdate;
    }

    public UpdateBookingDTO dtoFromObject(Booking booking){
        UpdateBookingDTO bookingDTO = new UpdateBookingDTO();
        bookingDTO.setVehicleId(booking.getVehicle().getId());
        bookingDTO.setDateTime(booking.getDropOffDate());
        bookingDTO.setServices(booking.getServices());
        return bookingDTO;
    }

}