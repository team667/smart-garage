package com.telerikacademy.smartgarage.models.user;

import com.telerikacademy.smartgarage.services.contracts.UserRoleService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserService userService;
    private final UserRoleService userRoleService;

    @Autowired
    public UserMapper(UserService userService, UserRoleService userRoleService) {
        this.userService = userService;
        this.userRoleService = userRoleService;
    }

    public User createDtoToObject(CreateUserDTO userDTO) {
        User user = new User();

        user.setUsername(userDTO.getUsername());
        user.setPassword(RandomString.make(10));
        user.setEmail(userDTO.getEmail());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        UserRole role = userRoleService.getById(1);
        user.setUserRole(role);

        return user;
    }

    public User updateDtoToObject(UpdateUserDTO userDTO, User userToUpdate) {

        if (userDTO.getUsername() != null) userToUpdate.setUsername(userDTO.getUsername());
        else userToUpdate.setUsername(userToUpdate.getUsername());

        if (userDTO.getEmail() != null) userToUpdate.setEmail(userDTO.getEmail());
        else userToUpdate.setEmail(userToUpdate.getEmail());

        if (userDTO.getPassword() != null) userToUpdate.setPassword(userDTO.getPassword());
        else userToUpdate.setPassword(userToUpdate.getPassword());

        if (userDTO.getPhoneNumber() != null) userToUpdate.setPhoneNumber(userDTO.getPhoneNumber());
        else userToUpdate.setPhoneNumber(userToUpdate.getPhoneNumber());

        if (userDTO.getRoleId() != null) {
            if (userDTO.getRoleId() != 0) {
                UserRole userRole = userRoleService.getById(userDTO.getRoleId());
                userToUpdate.setUserRole(userRole);
            } else userToUpdate.setUserRole(userToUpdate.getUserRole());
        }

        return userToUpdate;
    }

    public UpdateUserDTO dtoFromObject(User user) {
        UpdateUserDTO updateUserDTO = new UpdateUserDTO();
        updateUserDTO.setUsername(user.getUsername());
        updateUserDTO.setEmail(user.getEmail());
        updateUserDTO.setPhoneNumber(user.getPhoneNumber());
        updateUserDTO.setRoleId(user.getUserRole().getId());
        return updateUserDTO;
    }

}