package com.telerikacademy.smartgarage.models.booking;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;
import java.time.LocalDate;

public class BookingFilterDTO {

    private Integer userId;
    private Integer modelId;
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private String sort;
    private String search;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    @Nullable
    public LocalDate getDate() {
        return date;
    }

    public void setDate(@Nullable LocalDate date) {
        this.date = date;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

}