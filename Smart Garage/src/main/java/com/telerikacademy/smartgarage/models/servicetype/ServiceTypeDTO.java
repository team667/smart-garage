package com.telerikacademy.smartgarage.models.servicetype;

import javax.validation.constraints.*;

public class ServiceTypeDTO {

    @NotNull(message = "Service name cannot be null")
    @NotEmpty(message = "Service name cannot be empty")
    private String name;

    @Positive(message = "Please select a category from the dropdown menu")
    private int categoryId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

}