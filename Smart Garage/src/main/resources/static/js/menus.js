const modalBtns = document.getElementsByClassName('modal-button');
const modalBgs = document.getElementsByClassName('modal-bg');
const modals = document.getElementsByClassName('modal');
const modalCloseBtns = document.getElementsByClassName('modal-close');

for (let i = 0; i < modalBtns.length; i++) {
    let button = modalBtns[i];
    let modalBg = modalBgs[i];
    let modal = modals[i];
    let modalClose = modalCloseBtns[i];
    button.addEventListener("click", function () {
        modalBg.classList.toggle('bg-active');
        modal.classList.toggle('modal-active');
    })
    modalClose.addEventListener('click',function () {
        modalBg.classList.toggle('bg-active');
        modal.classList.toggle('modal-active');
    })
}

function menuToggle() {
    const toggleMenu = document.querySelector('.menu');
    toggleMenu.classList.toggle('active')
}

function expand(flag) {
    const brandMenu = document.getElementById('brand-menu');
    const modelMenu = document.getElementById('model-menu');
    const categoriesMenu = document.getElementById('categories-menu');
    if (flag) {
        let status;
        if (categoriesMenu.style.display == "none") {
            status = true;
        } else {
            status = false;
        }
        if (status === false) {
            categoriesMenu.style.display = "none";
        } else {
            categoriesMenu.style.display = "block"
        }
    } else {
        let status;
        if (brandMenu.style.display == "none") {
            status = true;
        } else {
            status = false;
        }
        if (status === false) {
            brandMenu.style.display = "none";
            modelMenu.style.display = "none";
        } else {
            brandMenu.style.display = "block"
            modelMenu.style.display = "block"
        }
    }
}