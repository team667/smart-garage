package com.telerikacademy.smartgarage;

import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.user.UserRole;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

public class Helpers {

    public static User createMockCustomer() {
        return testUser("Customer");
    }
    public static User createMockEmployee() {
        User user = testUser("Employee");
        user.setId(2);
        return user;
    }

    public static User testUser(String role){
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setPhoneNumber("0894555666");
        if(role.equals("Customer")) {
            mockUser.setUserRole(createMockRole("Customer"));
        }
        if(role.equals("Employee")){
            mockUser.setUserRole(createMockRole("Employee"));
        }
        return mockUser;
    }

    public static Optional<String> mockParam(){
        return  Optional.of("Bate Vankta e dobro parche");
    }


    public static UserRole createMockRole(String role) {
        var mockRole = new UserRole();
        if(role.equals("Employee")){
            mockRole.setId(2);
            mockRole.setName(role);
        }
        if(role.equals("Customer")){
            mockRole.setId(1);
            mockRole.setName(role);
        }

        return mockRole;
    }


    public static Brand mockBrand() {
        Brand brand = new Brand();
        brand.setId(1);
        brand.setName("brand");
        return brand;
    }

    public static Model mockModel() {
        Model model = new Model();
        model.setId(1);
        model.setName("model");
        model.setBrand(mockBrand());

        return model;
    }

    public static Vehicle mockVehicle(){
        Vehicle vehicle = new Vehicle();
        vehicle.setId(1);
        vehicle.setVin("x".repeat(16));
        vehicle.setBrand(mockBrand());
        vehicle.setModel(mockModel());
        vehicle.setOwner(createMockCustomer());
        vehicle.setYearOfCreation(LocalDate.of(2002,12,12));
        vehicle.setLicensePlate("CB7777CB");

        return vehicle;
    }

    public static Booking mockBooking(){
        Booking  booking = new Booking();
        booking.setId(1);
        booking.setCustomer(createMockCustomer());
        booking.setServices(new ArrayList<>());
        booking.setVehicle(mockVehicle());
        booking.setDropOffDate(LocalDate.of(2022, 4,24));

        return booking;
    }

    public static ServiceCategory mockServiceCategory(){
        ServiceCategory serviceCategory = new ServiceCategory();
        serviceCategory.setId(5);
        serviceCategory.setServices(new ArrayList<>());
        serviceCategory.setName("Test Category");
        return serviceCategory;
    }

    public static ServiceType mockServiceType() {

        ServiceType serviceType = new ServiceType();
        serviceType.setId(5);
        serviceType.setName("testName");
        serviceType.setServiceCategory(mockServiceCategory());

        return serviceType;
    }

    public static Service mockService(){

        Service service = new Service();
        service.setId(1);
        service.setServiceType(mockServiceType());
        service.setBooking(mockBooking());
        service.setPrice(100);

        return service;
    }
}
