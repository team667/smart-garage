package com.telerikacademy.smartgarage.services;


import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.user.UserRole;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UserRoleRepository;
import com.telerikacademy.smartgarage.services.contracts.UserRoleService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.telerikacademy.smartgarage.Helpers.mockParam;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository userRepository;

    @Mock
    UserRoleService userRoleService;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getById_ShouldReturn_WhenExist() {
//        UserRole userRole = userRoleService.getById(1);
        var mockRole = Helpers.createMockRole("Customer");

        Mockito.when(userRepository.getById(1))
                .thenReturn(new User(1, "Gosho", "email.abvcom", "password123", "0894555666", mockRole));

        User user = userService.getById(1);

        Assertions.assertEquals(1, user.getId());
        Assertions.assertEquals("Gosho", user.getUsername());
        Assertions.assertEquals("email.abvcom", user.getEmail());
        Assertions.assertEquals("password123", user.getPassword());
        Assertions.assertEquals("0894555666", user.getPhoneNumber());

    }

    @Test
    public void getBy_Should_Throw_When_NotExist() {
        var mockUser = Helpers.createMockCustomer();

        Mockito.when(userRepository.getById(2))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getById(2));
        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getByUsername(mockUser.getUsername()));
    }


    @Test
    public void getAll_SHouldCallRepo() {
        userService.getAll();
        Mockito.verify(userRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByUsername_SHouldCallRepo() {
        var mockUser = Helpers.createMockCustomer();
        userService.getByUsername(mockUser.getUsername());
        Mockito.verify(userRepository, Mockito.times(1)).getByUsername(mockUser.getUsername());
    }

    @Test
    public void UpdatePassword_SHouldCallRepo() {
        var mockUser = Helpers.createMockCustomer();
        userService.updatePassword(mockUser, "12345678");
        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    public void getByResetPasswordToken_SHouldCallRepo() {
        var mockUser = Helpers.createMockCustomer();
        userService.getByResetPasswordToken(mockUser.getResetPasswordToken());
        Mockito.verify(userRepository, Mockito.times(1)).findByResetPasswordToken(mockUser.getResetPasswordToken());
    }


    @Test
    public void search_ShouldThrow_WhenUserIsNotEmployee() {
        var user = Helpers.createMockCustomer();

        Assertions.assertThrows(UnauthorisedOperationException.class, () -> userService.search(mockParam(), user));
    }

    @Test
    public void search_Should_CallRepo_WhenUserIsEmployee() {
        var user = Helpers.createMockEmployee();
        var mockMessage = mockParam();

        userService.search(mockMessage, user);

        Mockito.verify(userRepository, Mockito.times(1)).search(mockMessage);
    }

    //Should Call getALL when Empty Search

    @Test
    public void create_Should_Throw_WhenDuplicateUsername() {
        var mockUser = Helpers.createMockCustomer();
//
        Mockito.when(userRepository.getByUsername(mockUser.getUsername())).thenReturn(mockUser);


        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.create(mockUser));
    }

    @Test
    public void create_Should_CallRepo_WhenEntityNotFound() {
        var mockUser = Helpers.createMockCustomer();

        Mockito.when(userRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "username", mockUser.getUsername()));

        Mockito.when(userRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(new EntityNotFoundException("User", "email", mockUser.getEmail()));

        userService.create(mockUser);


        Mockito.verify(userRepository, Mockito.times(1)).create(Mockito.any(User.class));
    }

    @Test
    public void update_Should_Throw_WhenUnauthorizedUser() {
        var mockUser = Helpers.createMockCustomer();
        var mockReq = Helpers.createMockCustomer();
        mockReq.setId(2);
        mockReq.setUsername("Gosho");


        Assertions.assertThrows(UnauthorisedOperationException.class, () -> userService.update(mockUser, mockReq));
    }

    @Test
    public void update_Should_Throw_WhenDuplicateEmail() {
        var User = Helpers.createMockCustomer();
        var mockReq = Helpers.createMockCustomer();


        Mockito.when(userRepository.getByEmail(User.getEmail()))
                .thenThrow(new DuplicateEntityException("User", "email", User.getEmail()));


        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.update(User, mockReq));
    }

    @Test
    public void update_Should_CallRepo() {
        var user = Helpers.createMockCustomer();
        var requester = Helpers.createMockCustomer();

        Mockito.when(userRepository.getByEmail(user.getEmail()))
                .thenReturn(user);

        userService.update(user, requester);

        Mockito.verify(userRepository, Mockito.times(1)).update(user);
    }

    @Test
    public void delete_Should_Throw_WhenUnauthorizedUser(){
        var mockUser =  Helpers.createMockCustomer();
        var mockReq = Helpers.createMockCustomer();
        mockReq.setId(2);
        mockReq.setUsername("Gosho");

        Assertions.assertThrows(UnauthorisedOperationException.class, () -> userService.delete(mockUser,mockReq));
    }

    @Test
    public void delete_Should_CallRepo_WhenUserIsAdmin(){
        var mockUser =  Helpers.createMockCustomer();
        var employee =  Helpers.createMockEmployee();

//        Mockito.when(userRepository.delete(mockUser)).thenReturn(mockUser);
        
        userService.delete(mockUser,employee);

        Mockito.verify(userRepository,Mockito.times(1)).delete(mockUser);
    }

    @Test
    public void delete_Should_CallRepo_UserIsAccountOwner(){
        var mockUser =  Helpers.createMockCustomer();
        var mockReq =  Helpers.createMockCustomer();

//        Mockito.when(userRepository.delete(mockUser)).thenReturn(mockUser);

        userService.delete(mockUser,mockReq);

        Mockito.verify(userRepository,Mockito.times(1)).delete(mockUser);

    }


    @Test
    public void search_ShouldThrow_WhenCustomer(){
        var user = Helpers.createMockCustomer();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> userService.search(mockParam(),user));
    }

    @Test
    public void search_ShouldCallRepo_WhenEmployee(){
        var user = Helpers.createMockEmployee();

        Mockito.when(userRepository.search(mockParam())).thenReturn((List<User>) Mockito.any(User.class));

        userService.search(mockParam(),user);
        Mockito.verify(userRepository,Mockito.times(1)).search(mockParam());
    }
















}
