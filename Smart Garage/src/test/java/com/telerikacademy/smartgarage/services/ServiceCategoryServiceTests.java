package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceCategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ServiceCategoryServiceTests {

    @Mock
    ServiceCategoryRepository serviceCategoryRepository;

    @InjectMocks
    ServiceCategoryServiceImpl categoryService;


    @Test
    public void getAll_ShouldCallRepo(){
        categoryService.getAll();

        Mockito.verify(serviceCategoryRepository,Mockito.times(1)).getAll();
    }

    @Test
    public void getById_ShouldCallRepo(){
        var serviceCategory = Helpers.mockServiceCategory();

        categoryService.getById(serviceCategory.getId());

        Mockito.verify(serviceCategoryRepository,Mockito.times(1)).getById(serviceCategory.getId());
    }

    @Test
    public void Create_shouldThrow_WhenUserIsNotEmployee(){
        var mockUser = Helpers.createMockCustomer();
        var serviceCategory = Helpers.mockServiceCategory();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> categoryService.create(serviceCategory,mockUser));
    }

    @Test
    public void Create_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var serviceCategory = Helpers.mockServiceCategory();

        categoryService.create(serviceCategory,mockUser);

       Mockito.verify(serviceCategoryRepository,Mockito.times(1)).create(serviceCategory);
    }

    @Test
    public void Update_shouldThrow_WhenUserIsNotEmployee(){
        var mockUser = Helpers.createMockCustomer();
        var serviceCategory = Helpers.mockServiceCategory();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> categoryService.update(serviceCategory,mockUser));
    }

    @Test
    public void Update_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var serviceCategory = Helpers.mockServiceCategory();

        categoryService.update(serviceCategory,mockUser);

        Mockito.verify(serviceCategoryRepository,Mockito.times(1)).update(serviceCategory);
    }

    @Test
    public void Delete_shouldThrow_WhenUserIsNotEmployee(){
        var mockUser = Helpers.createMockCustomer();
        var serviceCategory = Helpers.mockServiceCategory();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> categoryService.delete(serviceCategory,mockUser));
    }

    @Test
    public void Delete_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var serviceCategory = Helpers.mockServiceCategory();

        categoryService.delete(serviceCategory,mockUser);

        Mockito.verify(serviceCategoryRepository,Mockito.times(1)).delete(serviceCategory);
    }








}
