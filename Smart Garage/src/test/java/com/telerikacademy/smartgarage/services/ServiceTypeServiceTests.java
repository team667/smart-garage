package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceTypeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ServiceTypeServiceTests {

    @Mock
    ServiceTypeRepository serviceTypeRepository;

    @InjectMocks
    ServiceTypeServiceImpl serviceTypeService;

    @Test
    public void getAll_SHouldCallRepo(){
        serviceTypeService.getAll();

        Mockito.verify(serviceTypeRepository,Mockito.times(1)).getAll();
    }

    @Test
    public void getById_SHouldCallRepo(){
        var serviceType = Helpers.mockServiceType();

        serviceTypeService.getById(serviceType.getId());

        Mockito.verify(serviceTypeRepository,Mockito.times(1)).getById(serviceType.getId());
    }

    @Test
    public void filter_ShouldCallRepo(){
        serviceTypeService.filter(Optional.of("test"),Optional.empty());

        Mockito.verify(serviceTypeRepository,Mockito.times(1)).filter(Optional.of("test"),Optional.empty());

    }
    @Test
    public void Create_shouldThrow_WhenUserIsNotEmployee(){
        var mockUser = Helpers.createMockCustomer();
        var serviceType = Helpers.mockServiceType();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> serviceTypeService.create(serviceType,mockUser));
    }

    @Test
    public void Create_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var serviceType = Helpers.mockServiceType();

        serviceTypeService.create(serviceType,mockUser);

        Mockito.verify(serviceTypeRepository,Mockito.times(1)).create(serviceType);
    }

    @Test
    public void Update_shouldThrow_WhenUserIsNotEmployee(){
        var mockUser = Helpers.createMockCustomer();
        var serviceType = Helpers.mockServiceType();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> serviceTypeService.update(serviceType,mockUser));
    }

    @Test
    public void Update_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var serviceType = Helpers.mockServiceType();

        serviceTypeService.update(serviceType,mockUser);

        Mockito.verify(serviceTypeRepository,Mockito.times(1)).update(serviceType);
    }

    @Test
    public void Delete_shouldThrow_WhenUserIsNotEmployee(){
        var mockUser = Helpers.createMockCustomer();
        var serviceType = Helpers.mockServiceType();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> serviceTypeService.delete(serviceType,mockUser));
    }

    @Test
    public void Delete_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var serviceType = Helpers.mockServiceType();

        serviceTypeService.delete(serviceType,mockUser);

        Mockito.verify(serviceTypeRepository,Mockito.times(1)).delete(serviceType);
    }




}
