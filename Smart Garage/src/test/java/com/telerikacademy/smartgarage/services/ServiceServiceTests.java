package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ServiceServiceTests {

    @Mock
    ServiceRepository serviceRepository;

    @InjectMocks
    ServiceServiceImpl serviceService;

    @Test
    public void getAll_ShouldCallRepo(){
        serviceService.getAll();

        Mockito.verify(serviceRepository,Mockito.times(1)).getAll();
    }

    @Test
    public void getById_ShouldCallRepo(){
        var service = Helpers.mockService();

        serviceService.getById(service.getId());

        Mockito.verify(serviceRepository,Mockito.times(1)).getById(service.getId());
    }

    @Test
    public void Create_shouldThrow_WhenUserIsNotEmployeeAndIsNotOwner(){
        var mockUser = Helpers.createMockCustomer();
        mockUser.setId(3);
        mockUser.setUsername("test username");
        var mockService = Helpers.mockService();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> serviceService.create(mockService,mockUser));
    }

    @Test
    public void Create_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var mockService = Helpers.mockService();

        serviceService.create(mockService,mockUser);

        Mockito.verify(serviceRepository,Mockito.times(1)).create(mockService);
    }

    @Test
    public void Create_shouldCallRepo_WhenUserIsOwner(){
        var mockUser = Helpers.createMockCustomer();
        var mockService = Helpers.mockService();

        serviceService.create(mockService,mockUser);

        Mockito.verify(serviceRepository,Mockito.times(1)).create(mockService);
    }

    @Test
    public void Update_shouldThrow_WhenUserIsNotEmployeeAndIsNotOwner(){
        var mockUser = Helpers.createMockCustomer();
        mockUser.setId(3);
        mockUser.setUsername("test username");
        var mockService = Helpers.mockService();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> serviceService.update(mockService,mockUser));
    }

    @Test
    public void Update_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var mockService = Helpers.mockService();

        serviceService.update(mockService,mockUser);

        Mockito.verify(serviceRepository,Mockito.times(1)).update(mockService);
    }

    @Test
    public void Update_shouldCallRepo_WhenUserIsOwner(){
        var mockUser = Helpers.createMockCustomer();
        var mockService = Helpers.mockService();

        serviceService.update(mockService,mockUser);

        Mockito.verify(serviceRepository,Mockito.times(1)).update(mockService);
    }

    @Test
    public void Delete_shouldThrow_WhenUserIsNotEmployeeAndIsNotOwner(){
        var mockUser = Helpers.createMockCustomer();
        mockUser.setId(3);
        mockUser.setUsername("test username");
        var mockService = Helpers.mockService();


        Assertions.assertThrows(UnauthorisedOperationException.class,() -> serviceService.delete(mockService,mockUser));
    }

    @Test
    public void Delete_shouldCallRepo_WhenUserIsEmployee(){
        var mockUser = Helpers.createMockEmployee();
        var mockService = Helpers.mockService();

        serviceService.delete(mockService,mockUser);

        Mockito.verify(serviceRepository,Mockito.times(1)).delete(mockService);
    }

    @Test
    public void Delete_shouldCallRepo_WhenUserIsOwner(){
        var mockUser = Helpers.createMockCustomer();
        var mockService = Helpers.mockService();

        serviceService.delete(mockService,mockUser);

        Mockito.verify(serviceRepository,Mockito.times(1)).delete(mockService);
    }



}
