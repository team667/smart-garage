package com.telerikacademy.smartgarage.services;


import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.repositories.contracts.BrandRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BrandServiceTest {

    @Mock
    BrandRepository brandRepository;

    @InjectMocks
    BrandServiceImpl brandService;

    @Test
    public void getAllSHouldCallRepo(){
        brandService.getAll();
        Mockito.verify(brandRepository,Mockito.times(1)).getAll();
    }
    @Test
    public void getByIdSHouldCallRepo(){
        var brand = Helpers.mockBrand();

        brandService.getById(brand.getId());

        Mockito.verify(brandRepository,Mockito.times(1)).getById(brand.getId());
    }
    @Test
    public void getByNameSHouldCallRepo(){
        var brand = Helpers.mockBrand();

        brandService.getByName(brand.getName());

        Mockito.verify(brandRepository,Mockito.times(1)).getByName(brand.getName());
    }

    @Test
    public void createShouldCallRepo(){
        var brand = Helpers.mockBrand();

        Mockito.when(brandRepository.getByName(brand.getName())).thenThrow(EntityNotFoundException.class);

        brandService.create(brand);

        Mockito.verify(brandRepository,Mockito.times(1)).create(brand);
    }

    @Test
    public void create_ShouldThrowDuplicateException(){
        var brand = Helpers.mockBrand();

        Mockito.when(brandRepository.getByName(brand.getName())).thenReturn(brand);


        Assertions.assertThrows(DuplicateEntityException.class,() -> brandService.create(brand));
    }

    @Test
    public void updateShouldCallRepo(){
        var brand = Helpers.mockBrand();

        Mockito.when(brandRepository.getByName(brand.getName())).thenThrow(EntityNotFoundException.class);

        brandService.update(brand);

        Mockito.verify(brandRepository,Mockito.times(1)).update(brand);
    }

    @Test
    public void update_ShouldThrowDuplicateException(){
        var brand = Helpers.mockBrand();


        Mockito.when(brandRepository.getByName(brand.getName()))
                .thenThrow(new DuplicateEntityException("Brand", "name", brand.getName()));


        Assertions.assertThrows(DuplicateEntityException.class, () -> brandService.update(brand));
    }

    @Test
    public void delete_Should_CallRepo(){
        var brand = Helpers.mockBrand();

        brandService.delete(brand);

        Mockito.verify(brandRepository,Mockito.times(1)).delete(brand);
    }











}
