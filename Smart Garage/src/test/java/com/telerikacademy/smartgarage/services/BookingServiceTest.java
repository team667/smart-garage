package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.repositories.contracts.BookingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class BookingServiceTest {

    @Mock
    BookingRepository bookingRepository;

    @InjectMocks
    BookingServiceImpl bookingService;

    @Test
    public void getAllShouldCallRepo(){
        bookingService.getAll();

        Mockito.verify(bookingRepository,Mockito.times(1)).getAll();
    }

    @Test
    public void getByIdShouldCallRepo(){
        var booking = Helpers.mockBooking();

        bookingService.getById(booking.getId());

        Mockito.verify(bookingRepository,Mockito.times(1)).getById(booking.getId());
    }

    @Test
    public void CreateSHouldCallRepo(){
        var booking = Helpers.mockBooking();

        bookingService.create(booking);

        Mockito.verify(bookingRepository,Mockito.times(1)).create(booking);
    }

    @Test
    public void UpdateShouldThrowWhenUserIsNotEmployeeOrOwner(){
        var booking = Helpers.mockBooking();
        var requester = Helpers.createMockCustomer();
        requester.setId(3);
        requester.setUsername("test");


        Assertions.assertThrows(UnauthorisedOperationException.class,() ->  bookingService.update(booking,requester));
    }

    @Test
    public void UpdateShouldCallRepoWhenUserIsOwner(){
        var booking = Helpers.mockBooking();
        var requester = Helpers.createMockCustomer();

        bookingService.update(booking,requester);

        Mockito.verify(bookingRepository,Mockito.times(1)).update(booking);
    }

    @Test
    public void UpdateShouldCallRepoWhenUserIsEmployee(){
        var booking = Helpers.mockBooking();
        var requester = Helpers.createMockEmployee();

        bookingService.update(booking,requester);

        Mockito.verify(bookingRepository,Mockito.times(1)).update(booking);
    }

    @Test
    public void DeleteShouldThrowWhenUserIsNotEmployeeOrOwner(){
        var booking = Helpers.mockBooking();
        var requester = Helpers.createMockCustomer();
        requester.setId(3);
        requester.setUsername("test");


        Assertions.assertThrows(UnauthorisedOperationException.class,() ->  bookingService.delete(booking,requester));
    }

    @Test
    public void DeleteShouldCallRepoWhenUserIsOwner(){
        var booking = Helpers.mockBooking();
        var requester = Helpers.createMockCustomer();

        bookingService.delete(booking,requester);

        Mockito.verify(bookingRepository,Mockito.times(1)).delete(booking);
    }

    @Test
    public void DeleteShouldCallRepoWhenUserIsEmployee(){
        var booking = Helpers.mockBooking();
        var requester = Helpers.createMockEmployee();

        bookingService.delete(booking,requester);

        Mockito.verify(bookingRepository,Mockito.times(1)).delete(booking);
    }

//    @Test
//    public void FilterShouldCallRepo(){
//        var booking = Helpers.mockBooking();
//
//        bookingService.filter(Optional.of(1), Optional.empty(),Optional.empty(),booking.getCustomer());
//
//        Mockito.verify(bookingRepository,Mockito.times(1)).filter(Optional.of(1), Optional.empty(),Optional.empty(),booking.getCustomer());
//
//    }







}
