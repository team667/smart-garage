package com.telerikacademy.smartgarage.services;


import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ModelServiceTests {

    @Mock
    ModelRepository modelRepository;

    @InjectMocks
    ModelServiceImpl modelService;


    @Test
    public void getAllSHouldCallRepo(){
        modelService.getAll();
        Mockito.verify(modelRepository,Mockito.times(1)).getAll();
    }
    @Test
    public void getByIdSHouldCallRepo(){
        var model = Helpers.mockModel();

        modelService.getById(model.getId());

        Mockito.verify(modelRepository,Mockito.times(1)).getById(model.getId());
    }
    @Test
    public void getByBrandSHouldCallRepo(){
        var model = Helpers.mockModel();

        modelService.getByBrandId(model.getBrand().getId());

        Mockito.verify(modelRepository,Mockito.times(1)).getByBrandId(model.getBrand().getId());
    }

    @Test
    public void createShouldCallRepo(){
        var model = Helpers.mockModel();

        Mockito.when(modelRepository.getByName(model.getName())).thenThrow(EntityNotFoundException.class);

        modelService.create(model);

        Mockito.verify(modelRepository,Mockito.times(1)).create(model);
    }

    @Test
    public void create_ShouldThrowDuplicateException(){
        var model = Helpers.mockModel();

        Mockito.when(modelRepository.getByName(model.getName())).thenReturn(model);


        Assertions.assertThrows(DuplicateEntityException.class,() -> modelService.create(model));
    }

    @Test
    public void updateShouldCallRepo(){
        var model = Helpers.mockModel();

        Mockito.when(modelRepository.getByName(model.getName())).thenThrow(EntityNotFoundException.class);

        modelService.update(model);

        Mockito.verify(modelRepository,Mockito.times(1)).update(model);
    }

    @Test
    public void update_ShouldThrowDuplicateException(){
        var model = Helpers.mockModel();


        Mockito.when(modelRepository.getByName(model.getName()))
                .thenThrow(new DuplicateEntityException("Brand", "name", model.getName()));


        Assertions.assertThrows(DuplicateEntityException.class, () -> modelService.update(model));
    }

    @Test
    public void deleteSHouldCallRepo(){
        var model = Helpers.mockModel();

        modelService.delete(model);

        Mockito.verify(modelRepository,Mockito.times(1)).delete(model);
    }





}
