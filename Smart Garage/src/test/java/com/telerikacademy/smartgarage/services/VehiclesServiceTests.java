package com.telerikacademy.smartgarage.services;


import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class VehiclesServiceTests {

    @Mock
    VehicleRepository vehicleRepository;

    @InjectMocks
    VehicleServiceImpl vehicleService;

    @Test
    public void getAllSHouldCallRepo(){
        vehicleService.getAll();
        Mockito.verify(vehicleRepository,Mockito.times(1)).getAll();
    }
    @Test
    public void getByIdSHouldCallRepo(){
        var vehicle = Helpers.mockVehicle();

        vehicleService.getById(vehicle.getId());

        Mockito.verify(vehicleRepository,Mockito.times(1)).getById(vehicle.getId());
    }
    @Test
    public void getByBrandSHouldCallRepo(){
        var vehicle = Helpers.mockVehicle();
        var mockUser = Helpers.createMockCustomer();

        vehicleService.getByUserId(mockUser.getId());

        Mockito.verify(vehicleRepository,Mockito.times(1)).getByUserId(mockUser.getId());
    }

    @Test
    public void FilterShouldThrowWhenUserIsNotEmployee(){
        var mockUser = Helpers.createMockCustomer();
        mockUser.setId(3);
        mockUser.setUsername("Mario");
        var mockRequester = Helpers.createMockCustomer();

        Assertions.assertThrows(UnauthorisedOperationException.class,() -> vehicleService.filter(Optional.of(mockUser.getId()), Optional.empty(),mockRequester));
    }

    @Test
    public void FilterShouldCallRepoWhenUserIsEmployee(){
        var mockEmployee = Helpers.createMockEmployee();
        var mockUser = Helpers.createMockCustomer();

        vehicleService.filter(Optional.of(mockUser.getId()),Optional.empty(),mockEmployee);

        Mockito.verify(vehicleRepository,Mockito.times(1)).filter(Optional.of(mockUser.getId()),Optional.empty());
    }

    @Test
    public void CustomerSearchShouldCallRepo(){
        var user = Helpers.createMockCustomer();
        var username = Helpers.mockParam();
        vehicleService.customerSearch(username,user.getId());
        Mockito.verify(vehicleRepository,Mockito.times(1)).customerSearch(username, user.getId());
    }
    @Test
    public void AdminSearchShouldCallRepo(){
        var user = Helpers.createMockCustomer();
        var username = Helpers.mockParam();
        vehicleService.adminSearch(username);
        Mockito.verify(vehicleRepository,Mockito.times(1)).adminSearch(username);
    }

    @Test
    public void createShouldCallRepo(){
        var vehicle = Helpers.mockVehicle();

        Mockito.when(vehicleRepository.getByVin(vehicle.getVin())).thenThrow(EntityNotFoundException.class);

        vehicleService.create(vehicle);

        Mockito.verify(vehicleRepository,Mockito.times(1)).create(vehicle);
    }

    @Test
    public void create_ShouldThrowDuplicateException(){
        var vehicle = Helpers.mockVehicle();

        Mockito.when(vehicleRepository.getByVin(vehicle.getVin())).thenReturn(vehicle);


        Assertions.assertThrows(DuplicateEntityException.class,() -> vehicleService.create(vehicle));
    }

    @Test
    public void updateShouldCallRepo(){
        var vehicle = Helpers.mockVehicle();

        Mockito.when(vehicleRepository.getByVin(vehicle.getVin())).thenThrow(EntityNotFoundException.class);

        vehicleService.update(vehicle);

        Mockito.verify(vehicleRepository,Mockito.times(1)).update(vehicle);
    }

    @Test
    public void update_ShouldThrowDuplicateException(){
        var vehicle = Helpers.mockVehicle();


        Mockito.when(vehicleRepository.getByVin(vehicle.getVin()))
                .thenThrow(new DuplicateEntityException("Vehicle", "vin", vehicle.getVin()));


        Assertions.assertThrows(DuplicateEntityException.class, () -> vehicleService.update(vehicle));
    }
    @Test
    public void deleteSHouldCallRepo(){
        var vehicle = Helpers.mockVehicle();

        vehicleService.delete(vehicle);

        Mockito.verify(vehicleRepository,Mockito.times(1)).delete(vehicle);
    }




}
